<?php
/**
 * 活动分类
 */
namespace app\common\model;

use think\Model;

class ActivityClass extends Model
{
    protected $pk                 = 'id';
    protected $autoWriteTimestamp = 'int';

    public function Activity()
    {
        return $this->belongsTo('Activity', 'activity_id');
    }

}
