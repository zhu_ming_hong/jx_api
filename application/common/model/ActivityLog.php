<?php
/**
 * 活动分类
 */
namespace app\common\model;

use think\Model;

class ActivityLog extends Model
{
    protected $pk                 = 'id';
    protected $autoWriteTimestamp = 'int';
    //活动关联
    public function Activity()
    {
        return $this->belongsTo('Activity', 'active_id');
    }
    public function Member()
    {
        return $this->belongsTo('Member', 'uid');
    }
    
    public function Music()
    {
        return $this->belongsTo('Music', 'item_id');
    }

}
