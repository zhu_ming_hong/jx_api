<?php
namespace app\common\model;

use think\Model;

class Member extends Model
{
    protected $autoWriteTimestamp = true;

    public function setTipAttr($value)
    {
        return is_array($value) && !empty($value) ? implode(',', $value) : '';
    }
    public function getTipAttr($value)
    {
        return '' != $value ? explode(',', $value) : [];
    }

//    public function getSexAttr($value)
    //    {
    //        $txt = [0 => '女', 1 => '男'];
    //        return isset($txt[$value]) ? $txt[$value] : '--';
    //    }

    public function getStatusAttr($value)
    {
        $txt = [0 => '禁用', 1 => '启用'];
        return isset($txt[$value]) ? $txt[$value] : '--';
    }

    public function getRegTimeAttr($value)
    {
        return date('Y-m-d H:i:s', $value);
    }

    public static function slt()
    {
        return self::where('state', 1)->column('nickname', 'id');
    }
}
