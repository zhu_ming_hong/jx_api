<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/18
 * Time: 14:14
 */

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\model\User as UserModel;
use app\api\model\Node as NodeMode ;
use app\api\model\Node as NodeModel;
use app\api\validate\regQQUser;
use app\api\validate\SelectScenicContent;
use app\api\validate\editUser;
use app\api\validate\AddUser;
use app\api\validate\GetUser;
use app\api\validate\GetOpen;
use app\api\validate\delTeamAll;
use app\api\validate\UserRegister;
use app\api\validate\SendMessage;
use app\api\validate\AppForgePassword;
use app\api\validate\Userlogin;
use app\api\validate\saveLive;
use app\api\validate\addTeamOne;
use app\lib\exception\BaseException;
use app\lib\exception\SuccessMessage;
use aliyun\api_demo\SmsDemo;
use GuzzleHttp\Psr7\Request;
use think\Db;

class User extends BaseController
{
    /**
     * 登录小程序
     *
     */
    public function UserLoginB(){
        (new GetOpen())->goCheck(); //参数验证
        $products = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取登录用户信息';
        $info = $user->SelectOpen($products['openid']);
        if (!empty($info)) {
            $apiData = $res->apiData(0, $api, $apiName);//获取用户信息成功
            $apiData['Data'] = $info;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
            return json($apiData);
        }
    }
    /**
     * 注册小程序
     */
    public function UserRegisterB(){
        (new AddUser())->goCheck(); //参数验证
        $products = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '注册用户信息';
        $info = $user->addUserB($products);
        if (!empty($info)) {
            $apiData = $res->apiData(0, $api, $apiName);//获取用户信息成功
            $apiData['Data'] = $info;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
            return json($apiData);
        }
    }
    /**获取注册用户
     * @return \think\response\Json
     */
    public function selectUser()
    {
        (new GetUser())->goCheck(); //参数验证
        $products = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取用户信息列表';
        $status = $products['status'];
        $info = $user->SelectUser($products['user_id'],$status);
        if (!empty($info)) {
            $apiData = $res->apiData(0, $api, $apiName);//获取用户信息成功
            $apiData['Data'] = $info;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
            return json($apiData);
        }
    }
    /**获取注册离线用户
     * @return \think\response\Json
     */
    public function selectUserClose()
    {
        (new GetUser())->goCheck(); //参数验证
        $products = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取用户信息列表';
        $info = $user->SelectUserClose($products['user_id']);
        if (!empty($info)) {
            $apiData = $res->apiData(0, $api, $apiName);//获取用户信息成功
            $apiData['Data'] = $info;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
            return json($apiData);
        }
    }
    /**
     *修改用户信息
     * @return \think\response\Json
     */
    public function editUser()
    {
        (new editUser())->goCheck(); //参数验证
        $products = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $apiName = '上传图片';
        $api = $this->api;
        $file = $this->request->file('img');
        $date=date("Ymd");
        if($file){
            $info = $file->move(ROOT_PATH . 'public/uploads/user/');
            if($info){
                $img = '';
                // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
                $img=$info->getFilename();
                $products['img']= '/uploads/user/'.$date."/".$img;
            }
        }
        $apiName = '编辑用户信息';
        if(!empty($products['pwd'])){
            $products['password'] = trim(md5(hash('sha256', $products['pwd'] )));
            unset($products['pwd']);
        }elseif (empty($products['password'] )){
            unset($products['pwd']);
        }
        $info = $user->editUser($products);
        if (!empty($info)) {
            $Login=[];
            $Login = $user->SelectUserFind($products['id']);
            $apiData = $res->apiData(0, $api, $apiName);//获取用户信息成功
            if(count($Login)>0){
                unset($Login['password']);
            }
            $apiData['Data'] = $Login;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
            return json($apiData);
        }
    }
    /*
     * 查询景点内容
     * */
    public function SelectScenicContent()
    {
        (new SelectScenicContent())->goCheck(); //参数验证
        $products = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取景点信息';
        $info = $user->SelectScenic($products['id']);
//        print_r($info);die;
        if (!empty($info)) {
            $apiData = $res->apiData(0, $api, $apiName);//获取景点信息成功
            $apiData['Data'] = $info;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取景点信息失败
            return json($apiData);
        }

    }
    /*
     * 用户注册
     * */
    public function UserRegister()
    {
        (new UserRegister())->goCheck(); //参数验证
        $products = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '注册';
        $phone = $products['phone'];//手机号
         $nickname = $products['nickname'];//昵称
//        $fitness = $products['fitness'];//昵称
//        $age = $products['age'];//昵称
        $fistpassword='';
        $lastpassword='';
        if(empty($products['openid'])){
            $fistpassword = trim(md5(hash('sha256', $products['fistpassword'] )));//第一次输入的登录密码
            $lastpassword = trim(md5(hash('sha256', $products['lastpassword'] )));//再次输入的登录密码
        }
        $name = $user->UserLogin($products['phone']);
        if (!empty($name)) {  //用户已存在，不能重复添加
            $apiData = $res->apiData(20024, $api, $apiName);//该用户已存在
            return json($apiData);
        } else {  //用户不存在，可进行添加

            if ($fistpassword == $lastpassword) { //两次密码输入正确，进行添加数据
                //添加用户
                $Register = $user->UserRegister($phone, $fistpassword,$nickname, $products['openid']);
                if (empty($Register)) {
                    $apiData = $res->apiData(400, $api, $apiName);//注册失败
                    return json($apiData);
                } else {
                    $apiData = $res->apiData(0, $api, $apiName);//注册成功
                    return json($apiData);
                }
            } else {
                $apiData = $res->apiData(20023, $api, $apiName);//两次输入的密码不一致
                return json($apiData);
            }
        }
    }
    /*
     * 用户登录
     * */
    public function UserLogin()
    {
        (new Userlogin())->goCheck();#参数验证
        $products = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '登录';
        //trim() 去掉空格
        $phone = trim($products['phone']);
        $password = trim($products['password']);
        $Login = $user->UserLogin($phone);
//        print_r($Login);die;
        if (empty($Login)) {
            $apiData = $res->apiData(20012, $api, $apiName);//该用户不存在
            return json($apiData);
        } else {
            $password = md5(hash('sha256', $password));//密码加密
            if ($Login['password'] == $password) { #密码相等登录成功
                unset($Login['password']);
                $api['Data'] = $Login;
                $apiData = $res->apiData(0, $api, $apiName);//登录成功
                return json($apiData);

            } else {#密码不相等，登录失败
                $apiData = $res->apiData(20020, $api, $apiName);
                return json($apiData);
            }
        }
    }
    /*
     * 验证码的发送
     * */
    public function sendCode($mobile, $mbid, $rand)
    {
        $servantname = $rand;
        $username = $rand;
        $sms = new SmsDemo();
//        print_r($rand);print_r($mobile);dump($mbid);die;
        $send = $sms->sendSms($mobile, $mbid, $servantname, $rand, $username);
        $result = json_decode(json_encode($send), true);
        return $result;
    }
    /*
    * 发送验证码
    * */
    public function SendMessage()
    {
        (new SendMessage())->goCheck();#参数验证
        $products = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '发送短信';
        $mbbc = $products['mbbc'];
        $mobile = $products['mobile'];
        $rand = rand(1000, 9999);//验证码
        $result = $user->getmbmc($mbbc);//获取模板信息

        if ($result) {
            $hqdqsj = $user->getdxyzm($mobile);//获取到期时间
            if (empty($hqdqsj)) {//第一次发送短信
                try {
                    $fssms = $this->sendCode($mobile, $result['dsfmbid'], $rand);//发送短信
                } catch (Exception $e) {
                    $apiData = $res->apiData(10013, $api, $apiName);//短信发送失败
                    return json($apiData);
                }
            } else {
                //判断验证码是否过期
//                if (strtotime($hqdqsj["dqsj"]) <= time()) {//已过期重新发送
                try {
                    $fssms = $this->sendCode($mobile, $result['dsfmbid'], $rand);//发送短信
                } catch (Exception $e) {
                    $apiData = $res->apiData(10013, $api, $apiName);//短信发送失败
                    return json($apiData);
                }
//                } else {//未过期，提示稍后发送
//                    $gqsj = $hqdqsj["dqsj"];
//                    $end_time = strtotime($gqsj);
//                    $remain_time = $end_time - time();
//                    //剩余的小时
//                    $remain_hour = floor($remain_time / (60 * 60));
//                    //剩余的分钟数
//                    $remain_minute = floor(($remain_time - $remain_hour * 60 * 60) / 60);
//                    //剩余的秒数
//                    $remain_second = ($remain_time - $remain_hour * 60 * 60 - $remain_minute * 60);
////                    $apiData['msg']='请' . $remain_minute . '分钟' . $remain_second . '秒钟后再发';
//                    $apiData['msg']='消息未过期请稍后再发';
//                    $apiData['error_code'] =300;
//                    return json($apiData);
//                }
            }
//            print_r($fssms);die;
            if (!empty($fssms)) {//短信发送成功，添加短信记录
                if ($fssms['Message'] == 'OK' && $fssms['Code'] == 'OK') {
                    $tjsj = date('Y-m-d H:i:s', time());
                    $dqsj = date('Y-m-d H:i:s', strtotime("+5 minute"));
                    $data = array(
                        'id' => $this->guid_16(),
                        'mobile' => $mobile,//手机号
                        'yzm' => $rand,//验证码
                        'mbbc' => $result['mbbc'],//模板名称
                        'dxnr' => $rand,//模板内容
                        'tjsj' => $tjsj,//添加时间
                        'dqsj' => $dqsj,//到期时间
                    );
                    $insedxjl = $user->adddxfsjl($data);//添加短息记录
                    if ($insedxjl) {
                        $apiData = $res->apiData(0, $api, $apiName);//短信发送成功
                        $apiData['Data']['code'] = $rand;
                        return json($apiData);
                    } else {
                        $apiData = $res->apiData(10013, $api, $apiName);//短信发送失败
                        return json($apiData);
                    }

                } else {
                    $apiData = $res->apiData(10013, $api, $apiName);//短信发送失败
                    return json($apiData);
                }
            } else {
                $apiData = $res->apiData(10013, $api, $apiName);//短信发送失败
                return json($apiData);
            }
        } else {
            $apiData = $res->apiData(10013, $api, $apiName);//短信发送失败
            return json($apiData);
        }
    }
    /*
   * app忘记密码
   * */
    public function  AppForgePassword(){
            $post = input('post.');
            (new AppForgePassword())->goCheck();#验证参数id
            $res = new BaseException();//实例化类
            $api = $this->api;
            $phone = $post['phone'];//手机号
            $user = new UserModel;
            $yzm = $post['yzm'];
            $fistpassword = trim(md5(hash('sha256', $post['fistpassword'])));//第一次输入的登录密码
            $lastpassword = trim(md5(hash('sha256', $post['lastpassword'])));//再次输入的登录密码
            $apiName = '修改密码';
            $users = $user->UserLogin($phone);
//            print_r($fistpassword);die;
            if($users){
                $getyzm = $user->Getyzm($phone);//查询验证码
//                print_r($getyzm);die;
                if ($getyzm) {
                    //获取验证码成功后，判断验证码是否相等
                    if ($getyzm['yzm'] == $yzm) {

                        if ($fistpassword == $lastpassword) { //两次密码输入正确，进行添加数据
                            //修改密码
                            $updatepwd = $user->UpdatePwd($phone, $fistpassword);
                            if (empty($updatepwd)) {
                                $apiData = $res->apiData(400, $api, $apiName);//修改密码失败
                                return json($apiData);
                            } else {
                                $apiData = $res->apiData(0, $api, $apiName);//修改密码成功
                                return json($apiData);
                            }
                        } else {
                            $apiData = $res->apiData(20023, $api, $apiName);//两次输入的密码不一致
                            return json($apiData);
                        }

                    } else {
                        $apiData = $res->apiData(10012, $api, $apiName);//验证码错误
                        return json($apiData);
                    }


                } else {
                    $apiData = $res->apiData(20021, $api, $apiName);//数据不存在
                    return json($apiData);
                }

            }else{
                $apiData = $res->apiData(20012, $api, $apiName);//用户不存在
                return json($apiData);
            }


         }
    /**
     * 关于我们
     *
     */
    public function about(){
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取关于我们';
        $info = $user->SelectAbout();
        if (!empty($info)) {
            $apiData = $res->apiData(0, $api, $apiName);//获取用户信息成功
            $apiData['Data'] = $info;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
            return json($apiData);
        }
    }
    /***
     * 记录用户选择的线路
     */
    public function saveLive(){
        (new saveLive())->goCheck();#参数验证
        $lives = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '记录用户线路';
        $datas = $user::addLives($lives);
        if (!empty($datas)) {
            $apiData = $res->apiData(0, $api, $apiName); //添加线路
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
            return json($apiData);
        }
    }
    /***
     * 记录用户选择的线路
     */
    public function getsLive(){
        $lives = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '查询用户线路';
        $datas = $user::getsLive($lives);
        if (!empty($datas)) {
            $apiData = $res->apiData(0, $api, $apiName); //添加线路
            $apiData['Data'] = $datas;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
            return json($apiData);
        }
    }
    /***
     * 线路完成状态
     */
    public function editLives(){
        $lives = input();
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '修改线路完成状态';
        $datas = $user->editLives($lives);
        if (!empty($datas)) {
            $apiData = $res->apiData(0, $api, $apiName); //添加节点
            $apiData['Data'] = $datas;
            return json($apiData);
        } else {
            $apiData = $res->apiData(0, $api, $apiName); //添加节点
            $apiData['Data'] = $datas;
            return json($apiData);
        }
    }
    /***
     * 记录用户选择的线路
     */
    public function addNodes(){
        $lives = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '用户节点id';
        $datas = $user->addNodes($lives);
        if (!empty($datas)) {
            $apiData = $res->apiData(0, $api, $apiName); //添加节点
            $apiData['Data'] = $datas;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取节点
            return json($apiData);
        }
    }
    /***
     * 添加队员
     *
     */
    public function addTeam(){
        $posts = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '添加队员';
        $datas = $user->addTeam($posts);
        if (!empty($datas)) {
            $apiData = $res->apiData(0, $api, $apiName); //添加节点
            $apiData['Data'] = $datas;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取节点
            return json($apiData);
        }
    }
    /**
     * POST
     * 添加队员 一对一添加
     * 队长手动添加
     * 传递参数 队长user_id 队员真实姓名 name
     * lichen 2019/4/10
     */
    public function addTeamOne(){
        (new addTeamOne())->goCheck();
        $user_id = input();
        $res = new BaseException();//实例化类
        $user = new UserModel;
        $api=$this->api;
        $type =  db('team')->where('user_id',$user_id['user_id'])->select();
        if(empty($type)){
            $datas = ['user_id'=>$user_id['user_id']];
            db('team')->insert($datas);
        }
        $teamData = NodeModel::getTeam($user_id['user_id']);
        //根据队长id获取队员姓名
        $teamNameData = NodeModel::getTeamName($user_id['user_id']);
        $teamsName = $teamNameData['name'].'、'.$user_id['name'];
        //将新的姓名存入数据库
        $data = $user->updateTeamName($user_id['user_id'],$teamsName);
        if(!$data){
            $apiName = '更新失败';
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }
        //根据队长id获取队员姓名
        $teamNameData = NodeModel::getTeamName($user_id['user_id']);
        $teamNameDatas = explode('、',$teamNameData['name']);
        $apiName = '添加队员';
        $api['Data']['username']=  $teamNameDatas[0];
        //将队员信息组合成集合传递
        $teamName = [];
        for($i = 1,$j = 0;$i < count($teamNameDatas);$i++,$j++){
            $teamName[$j] = $teamNameDatas[$i];
        }
        $api['Data']['username1'] = $teamName;
        $apiData = $res->apiData(0, $api, $apiName);
        return json($apiData);
    }

    /**
     * 将对队伍的相同操作统一抽取
     */
    public function teamBase($user_id){
        //根据队长id获取队员姓名
        $teamNameData = NodeModel::getTeamName($user_id['user_id']);
        $teamNameDatas = explode('、',$teamNameData['name']);
    }
    /**
     *  删除队员
     *  根据队长user_id 队员name  删除队员
     *  参数 user_id  name
     *  单一删除
     */
    public function delTeam(){
        (new addTeamOne())->goCheck();
        $user_id = input('post.');
        $res = new BaseException();//实例化类
        $user = new UserModel;
        $api=$this->api;
        //根据队长id获取队员姓名
        $teamNameData = NodeModel::getTeamName($user_id['user_id']);
        $teamNameDatas = explode('、',$teamNameData['name']);
        if(!in_array($user_id['name'],$teamNameDatas)){
            $apiName = '删除队员';
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }
        $teams = '';
        //字符串拼接删除后的队伍数据存入数据库
        for($i = 0;$i < count($teamNameDatas);$i++){
            if($teamNameDatas[$i] != $user_id['name']){
                $teams.=$teamNameDatas[$i].'、';
            }
        }
        //除去最后一个 '、'号
        $teams = rtrim($teams, "、");
        //将新队员数据存入数据库
        $data = $user->updateTeamName($user_id['user_id'],$teams);
        if(!$data){
            $apiName = '删除队员';
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }
        //根据队长id获取队员姓名
        $teamNameData = NodeModel::getTeamName($user_id['user_id']);
        $teamNameDatas = explode('、',$teamNameData['name']);
        $apiName = '删除队员';
        $api['Data']['username']=  $teamNameDatas[0];
        //将队员信息组合成集合传递
        $teamName =[];
        for($i = 1,$j = 0;$i < count($teamNameDatas);$i++,$j++){
            $teamName[$j] = $teamNameDatas[$i];
        }
        $api['Data']['username1'] = $teamName;
        $apiData = $res->apiData(0, $api, $apiName);
        return json($apiData);

    }
    /**
     * 查询队长信息
     */
    public function getTeam(){
        $lives = input('post.');
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '查询队长信息';
        $datas = $user->getTeam($lives);
        if (!empty($datas)) {
            $apiData = $res->apiData(0, $api, $apiName); //添加线路
            $apiData['Data'] = $datas;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
            return json($apiData);
        }
    }
    /**
     * 查询组队信息
     */
    public function getTeamData(){
        $posts = input();
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '查询组队信息';
        $datas = $user->getTeamData($posts);
        if ($datas && !empty($datas)) {
         //查询队伍信息成功
        $teamNameData = NodeModel::getTeamName($posts['user_id']);
        $teamNameDatas = explode('、',$teamNameData['name']);
        $teamName  = [];//存队长信息
        foreach($datas[0] as $key=>$value){
            $teamName[$key] = $value;
        }
        $api['Data']['username'] = $teamName;
        $teamNames =[];//存队员信息
        for($i = 1,$j = 0;$i < count($teamNameDatas);$i++,$j++){
            $teamNames[$j] = $teamNameDatas[$i];
        }
        $api['Data']['username1'] = $teamNames;
        $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//查询队伍信息失败
            return json($apiData);
        }
    }
    /**
     * 查询组队信息
     */
    public function getTeamList(){
        $user = new UserModel;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '查询组队列表';
        $datas = $user->getTeamList();
        if (!empty($datas)) {
            $apiData = $res->apiData(0, $api, $apiName); //添加节点
            $apiData['Data'] = $datas;
            return json($apiData);
        } else {
            $apiData = $res->apiData(400, $api, $apiName);//获取节点
            return json($apiData);
        }
    }
    public function delTeamAll(){
        $ids = input('post.id');
        (new delTeamAll())->goCheck(); #验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $id = explode(',', $ids);
        $data = UserModel::delTeamAll($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }

    public function activity()
    {
        $page = input('page');
        $pageSize = input('size');
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取活动信息';
        $time = date('Y-m-d H:i:s',time());

        $start = ($page-1)* $pageSize;
        $data = UserModel::activity($start,$pageSize,$time);
        $api['Data'] = $data;
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据失败
        return json($apiData);
    }

    /***
     * 获取活动信息
     * 参数  查询条数 tatol
     * 页数 查询开始页数 limit 1  2  3
     */
    public function activity2()
    {
        $limit = input('post');
        $tatol = $limit['tatol'];
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取活动信息';
        //传递参数 1 2 3  1 就是 1- 10 条数据  2 就是 11-20 条数据  3 把剩余数据提交
        //分页 第一次 1 1-10条数据 第二次 2 11-20数据 第三次 提示已经是最新数据了  倒叙
        //数据库活动数据  id数量越大 活动开始时间越晚，即表示为最新活动
        //首页判断数据库活动数据数量
        // 判断活动结束时间是否大于当前时间  小于即活动已结束
        $time = date('Y-m-d H:i:s',time());
//        $num = UserModel::activitys($time);
        $num = UserModel::activitys($time);
//        print_r($num);exit;
        if($num < $tatol){
                $start = ($limit['limit']-1)* $tatol;
                $data = UserModel::activity($start,$tatol,$time);
                $api['Data'] = $data;
                $apiData = $res->apiData(0, $api, $apiName);//删除单条数据失败
                return json($apiData);
            //数量小于10 提示已经是最新了
        }elseif($num == $tatol){
            $start = ($limit['limit']-1)*$tatol;//16
            if($start  == 0){
                $data = UserModel::activity($start,$tatol,$time);
                $api['Data'] = $data;
                $apiData = $res->apiData(0, $api, $apiName);//删除单条数据失败
                return json($apiData);
            }else{
                $apiData = $res->apiData(20042, $api, $apiName);
                return json($apiData);
            }
            }else{
            $start = ($limit['limit']-1)* $tatol;
            //判断数据库是否还有数据
//            $num1 = $start + $tatol;
//            echo $num1;exit;  // 3  tatol 5  1
//            echo $num-$num1;exit;
            if(($num - $start) > 0 && ($num - $start) <= $tatol){
                $data = UserModel::activity($start,$tatol,$time);
                $api['Data'] = $data;
                $apiData = $res->apiData(0, $api, $apiName);
                return json($apiData);
            }elseif(($num - $start) > $tatol){
                $data = UserModel::activity($start,$tatol,$time);
                $api['Data'] = $data;
                $apiData = $res->apiData(0, $api, $apiName);
                return json($apiData);
            }else{
                $apiData = $res->apiData(20042, $api, $apiName);
                return json($apiData);
            }

            }
        }
    /**
     * 根据id获取活动详情
     */
    public function getActivity(){
    $id = input('post.');
    $res = new BaseException();//实例化类
    $api = $this->api;
    $apiName = '获取活动详情信息';
    $data = UserModel::activityOne($id['id']);
    if (!$data || empty($data)) {
        $apiData = $res->apiData(400, $api, $apiName);//获取活动详情失败
        return json($apiData);
    } else {
        //http://192.168.31.9/api/v1/User/getActivity?id=1 活动链接
		 $arr = explode(',',$data['nodes']);
        $data['nodes'] = count($arr);
        $api['Data'] = $data;
        $apiData = $res->apiData(0, $api, $apiName);//获取活动详情成功
//        $api['id'] =
        return json($apiData);
    }
}
/**
     * 点击点标，获取点标详情数据
     * 参数  activity_id  活动id
     *
     */
    public function getNodes(){
        $list = input('post.');
        //验证
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取点标详情信息';
        //根据线路id获取线路中所有节点
        $data = UserModel::getNodess($list['activity_id']);

        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//获取点标详情失败
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);//获取点标详情成功
            return json($apiData);
        }
    }

    /**
     * @param $id
     * @return string
     * 待定 李陈 2019/4/10
     */
    public function fenxiang($id){
        $id = input('post.');
        //获取本地ip
       $str = 'http://192.168.31.9/api/v1/User/fenxing?id='.$id['id'];
       return $str;
    }

    /**
     * app  QQ登录
     * 注册用户
     * 参数 nickname昵称
     *  img     头像是一个图片链接直接存入数据库
     *  openid  唯一标识
     */
    public function regQQUser(){
        //(new delTeamAll())->goCheck(); #验证参数id
        (new regQQUser())->goCheck();
        $list = input('post.');
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '添加QQ用户';
        $id = UserModel::addUserQQ($list['nickname'],$list['openid'],$list['img']);
        if($id){
            $data = Usermodel::getUserById($id);
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }else{
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }
    }

    /**
     * app  微信登录
     * 注册用户
     * 参数 nickname昵称
     *  img     头像是一个图片链接直接存入数据库
     *  openid  唯一标识
     */
    public function regWXUser(){
        (new regQQUser())->goCheck();//验证参数
        $list = input('post.');
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '添加微信用户';
        $id = UserModel::addUserQQ($list['nickname'],$list['openid'],$list['img']);
        if($id){
            $data = Usermodel::getUserById($id);
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }else{
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }

    }
    /**
     * 传递用户协议
     * 前端直接使用富文本编辑器进行添加修改
     * 然后存入数据库
     */
    public function getXY(){
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取用户协议';
        //字段名
        $name = 'content2';
        $result = UserModel::getMessage($name);
        if(!$result || empty($result)){
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }else{
            $api['Data'] = $result['content2'];
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }

    }
    /**
     * 传递关于我们
     */
    public function getAboutW(){
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取关于我们';
        //字段名
        $name = 'content';
        $result = UserModel::getMessage($name);
        if(!$result || empty($result)){
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }else{
            $api['Data'] = $result['content'];
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }
    }
    //详情页面 获取点标详情
    public function get_marker()
    {
        $id   = input('post.id');
        $res  =  DB::name('activity')->where('id',$id)->select();
        $data =  DB::name('admin_site')->whereIn('id',$res[0]['nodes'])->field('id,title,latitude,longitude')->select();
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '';
        $api['Data']  = $data;
        $apiData = $res->apiData(0, $api, $apiName);
        return json($apiData);
    }

    //首尔 发现活动
    public function get_activity()
    {
        $data =  DB::name('admin_activity')->field('id,title,address,name,introduction,img,desc')->select();
        $res = new BaseException();//实例化类
        $api['Data']  = $data; $apiName = '';
        $apiData = $res->apiData(0, $api, $apiName);
        return json($apiData);
    }

    //发现活动
    public function show_activity()
    {
        $id  = input('post.id');
        $res   =   DB::name('admin_activity')->where('id',$id)->find();
        $datas  =  Db::name('item_bank')->whereIn('id',$res['nodes'])->select();
        $s = new BaseException();//实例化类
        $data  =[
            'id'=>$res['id'],
            'title'=>$res['title'],
            'img'=>$res['img'],
            'answer'=>$datas
        ];
        $api['Data']  = $data; $apiName = '';
        $apiData = $s->apiData(0, $api, $apiName);
        return json($apiData);
    }

    //发现景区
    public function getScenic()
    {
        $data =  DB::name('admin_scenic')->field('id,title,address,name,introduction,img,desc')->select();
        $res  = new BaseException();//实例化类
        $api['Data']  = $data; $apiName = '';
        $apiData = $res->apiData(0, $api, $apiName);
        return json($apiData);
    }
    
    //发现景区 list
    public function get_Scenic_info()
    {
        $id     =  input('post.id');
        $res    =  DB::name('admin_scenic')->where('id',$id)->find();
        $datas  =  Db::name('item_bank_scenic')->whereIn('id',$res['nodes'])->select();
        $s = new BaseException();//实例化类
        $data  = [
            'id'=>$res['id'],
            'title'=>$res['title'],
            'img'=>$res['img'],
            'answer'=>$datas
        ];
        $api['Data']  = $data; $apiName = '';
        $apiData = $s->apiData(0, $api, $apiName);
        return json($apiData);
    }
    
    //完成线路 弹出奖章页
    public function complete_live()
    {
        $user_id= input('post.user_id');$live_id= input('post.live_id');
        DB::name('medal')->insert([
                'user_id'=>$user_id,'live_id'=>$live_id,
                'complete_time'=>date('Y-m-d h:i:s')]);
        $res = new BaseException();//实例化类
        $data = [
            'title'=>db('admin_scenic')->where('id',$live_id)->value('title'),
            'captain'=>db('user')->where('id',$user_id)->value('nickname'),
            'comment'=>'于'.date('Y-m-d h:i:s').'完成'.db('admin_scenic')->where('id',$live_id)->value('title')
        ];
        $api['Data']  = $data;
        $apiName = '';
        $apiData = $res->apiData(0, $api, $apiName);
        return json($apiData);
    }

    //完成线路 勋章
    public function complete_medal()
    {
        $user_id= input('post.user_id');$live_id= input('post.live_id');
        $res = DB::name('medal')->where('user_id',$user_id)->find();
        if (!empty($res)){
               DB::name('medal_xw')->insert([
                   'user_id'=>$user_id,'live_id'=>$live_id,
               ]);
        }
        $data  = [
            'live_id' =>  $res['live_id'],
            'medal'   =>  '/uploads/adminuser/20190418/test22.png',
        ];
        $ress = new BaseException();//实例化类
        $api['Data']  = $data;
        $apiName = '';
        $apiData = $ress->apiData(0, $api, $apiName);
        return json($apiData);
    }

    //

}
