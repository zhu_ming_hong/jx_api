<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/1/9
 * Time: 11:15
 */

namespace app\api\controller\v1;
use app\api\validate\Site as SiteValidate;
use app\api\validate\DelSite;
use app\api\controller\BaseController;
use app\lib\exception\BaseException;
use app\api\model\Site as SiteModel;

class Site extends BaseController
{
    public function getSite(){
        $data  = input('post.city_name');
        $data = SiteModel::getSite($data);
        $res = new BaseException();
        $api = $this->api;
        $apiName = '获取场馆列表';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }
    }

    /**
     *  根据用户输出的字段查找场馆
     *  模糊查询
     */
    public function getSiteMsg(){
        $msg = input('post.');
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '查找会馆';
        $site = new SiteModel();
        $data = $site->findLikeSite($msg['msg']);
        if(!$data || empty($data)){
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }else{
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }
    }
    //测试
    public function getSiteMsgs(){
        $msg = input('post.');
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '查找会馆';
        $site = new SiteModel();
        $sql = $site->findLikeSites($msg['msg']);
        echo $sql;
    }
    //添加场馆
    public function addSite(){
        (new SiteValidate())->goCheck();
        $res = new BaseException();//实例化类
        $apiName = '添加场馆';
        $api = $this->api;
        $data = input('post.');
        $site = new SiteModel();
        $status = $site->siteAdd($data);
        if (empty($status)) {
            $apiData = $res->apiData(400, $api, $apiName);//添加失败
            return json($apiData);
        } else {
            $apiData = $res->apiData(0, $api, $apiName);//添加成功
            return json($apiData);
        }
    }
    // 删除场馆信息
    public function delSite(){
        $id = input('post.id');
        (new DelSite)->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $data = SiteModel::delSite($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
    public function editSite(){
        $id = input('post.id');
        $data = input('post.');
        (new DelSite)->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '修改数据';
        $data = SiteModel::editSite($id,$data);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
    public function delSiteAll(){
        $id = input('post.id');
        (new DelSite())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $id = explode(',', $id);
        $data = SiteModel::delSite($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
}