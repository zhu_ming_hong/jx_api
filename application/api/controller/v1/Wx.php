<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/2/14
 * Time: 10:28
 */

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\lib\exception\BaseException;
use app\api\validate\Infos as InfosValidate;
use app\api\validate\Wx as WxValidate;
use app\api\model\Infos as InfosModel;
use app\api\model\Wx as WxModel;
class Wx extends BaseController
{
    public function addOpenid()
    {
        (new WxValidate())->goCheck();
        $res = new BaseException();
        $params = input('post.');
        $api = $this->api;
        $apiName = '添加openid';
        $hasOpenid = new WxModel();
        $info = $hasOpenid::hasOpenid($params['openid']);
        if(isset($info['id'])) {
            $apiData = $res->apiData(20024, $api, $apiName);
            return json($apiData);
        } else {
            $list = [];
            $list['phone'] = $params['phone'];
            $list['openid'] = $params['openid'];
            $list['name'] = $params['name'];
            $list['age'] = $params['age'];
            $inserts = $hasOpenid::addOpenid($list);
            if($inserts){
                $apiData = $res->apiData(0, $api, $apiName);
                $apiData['Data'] = $info;
                return json($apiData);
            } else {
                $apiData = $res->apiData(400, $api, $apiName);
                return json($apiData);
            }
        }
    }
    /**
     * 添加
     */
    public function addAges(){
        (new InfosValidate())->goCheck();
        $res = new BaseException();
        $params = input('post.');
        $api = $this->api;
        $apiName = '基本信息';
        $infos = new InfosModel();
        $openid = $params['openid'];
        $list=[];
        if(isset($params['name'])){
            $list['name'] = $params['name'];
        }
        if(isset($params['age'])){
            $list['age'] = $params['age'];
        }
        if(isset($params['phone'])){
            $list['phone'] = $params['phone'];
        }
        if(isset($params['body'])){
            $list['body'] = $params['body'];
        }
        $hasOpenid = new WxModel();
        $info = $hasOpenid::hasOpenid($params['openid']);
        if(!isset($info['id'])) {
            $apiData = $res->apiData(20012, $api, $apiName);
            return json($apiData);
        }
        $data = $infos::addData($openid,$list);
        if($data){
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        } else{
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }
    }
    /**
     * 查询场馆信息
     */
}