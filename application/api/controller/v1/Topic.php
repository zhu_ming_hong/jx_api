<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/1/9
 * Time: 11:15
 */

namespace app\api\controller\v1;
use app\api\validate\Topic as TopicValidate;
use app\api\validate\DelTopic;
use app\api\validate\GetTopic;
use app\api\validate\GetTopics;
use app\api\controller\BaseController;
use app\lib\exception\BaseException;
use app\api\model\Topic as TopicModel;

class Topic extends BaseController
{
    /**获取选择题
     * @return \think\response\Json
     */
    public function getChoice(){
        $list = input('post.');
        (new DelTopic)->goCheck();#验证参数id
        $data = TopicModel::getChoices($list['id']);
        $res = new BaseException();
        $api = $this->api;
        $apiName = '获选择题列表';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }
    }
    /**
     * 获取题库
     * @return \think\response\Json
     */
    public function getTopic(){
    $user_id = input('post.');
    (new GetTopic)->goCheck();#验证参数id
    $data = TopicModel::getTopic($user_id['site_id']);
    $res = new BaseException();
    $api = $this->api;
    $apiName = '获取线路列表';
    if (!$data || empty($data)) {
        $apiData = $res->apiData(400, $api, $apiName);
        return json($apiData);
    } else {
        $api['Data'] = $data;
        $apiData = $res->apiData(0, $api, $apiName);
        return json($apiData);
    }
}

    /**
     * 通过点标id获取题库
     * @return \think\response\Json
     */
    public function getTopics(){
        $user_id = input('post.');
        (new GetTopics)->goCheck();#验证参数id
        $data = TopicModel::getTopics($user_id['node_id']);
        $res = new BaseException();
        $api = $this->api;
        $apiName = '获取线路列表';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }
    }
    //添加场馆
    public function addTopic(){
        (new TopicValidate())->goCheck();
        $res = new BaseException();//实例化类
        $apiName = '添加题库';
        $api = $this->api;
        $data = input('post.');
        $Topic = new TopicModel();
        $status = $Topic->TopicAdd($data);
        if (empty($status)) {
            $apiData = $res->apiData(400, $api, $apiName);//添加失败
            return json($apiData);
        } else {
            $apiData = $res->apiData(0, $api, $apiName);//添加成功
            return json($apiData);
        }
    }
    // 删除场馆信息
    public function delTopic(){
        $id = input('post.id');
        (new DelTopic)->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $data = TopicModel::delTopic($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
    public function editTopic(){
        $id = input('post.id');
        $data = input('post.');
        (new DelTopic)->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '修改数据';
        $data = TopicModel::editTopic($id,$data);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
    public function delTopicAll(){
        $id = input('post.id');
        (new DelTopic())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $id = explode(',', $id);
        $data = TopicModel::delTopic($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
}