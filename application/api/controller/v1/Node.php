<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/1/9
 * Time: 11:15
 */

namespace app\api\controller\v1;
use app\api\validate\Node as NodeValidate;
use app\api\validate\DelNode;
use app\api\validate\GetNode;
use app\api\validate\GetNodes;
use app\api\validate\GetCert;
use app\api\model\User as UserModel;
use app\api\controller\BaseController;
use app\lib\exception\BaseException;
use app\api\model\Node as NodeModel;

class Node extends BaseController
{
    public function getNode(){
        $user_id = input('post.');
        (new GetNode)->goCheck();#验证参数id
        $data = NodeModel::getNode($user_id['user_id']);
        $res = new BaseException();
        $api = $this->api;
        $apiName = '获取线路列表';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }
    }

    /**
     * app 通过线路id获取节点
     * @return \think\response\Json
     */
    public function getNodes(){
        $user_id = input();
        (new GetNodes)->goCheck();#验证参数isd
        $data = NodeModel::getNodes($user_id['live_id']);
        $res = new BaseException();
        $api = $this->api;
        $apiName = '获取线路列表';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $api['node_id'] = "";
            // 选择线路添加
            if(isset($user_id['user_id'])&&!empty($user_id['user_id'])){
                $inset=[];
                $inset['live_id'] = $user_id['live_id'];
                $inset['user_id'] = $user_id['user_id'];
                $user = new UserModel;
                $datas = $user::addLives($inset);
                $NodeId = NodeModel::getNodeId($user_id['live_id']);
                $api['node_id'] = $NodeId;
                if (!empty($datas)) {
                    $apiData = $res->apiData(0, $api, $apiName); //添加线路
                    return json($apiData);
                } else {
                    $apiData = $res->apiData(400, $api, $apiName);//获取用户信息失败
                    return json($apiData);
                }
            }
            // 获取最新线路id
            $apiData = $res->apiData(0, $api, $apiName); //添加线路
            return json($apiData);
        }
    }
    /**
     * app 通过点击完成线路 传递线路live_id 用户user_id 获取奖励证书
     * 根據用戶user_id 獲取隊伍信息和隊伍完成時間并判斷隊伍狀態是否完成
     * @return \think\response\Json
     *
     * app 点击完成路线领取证书 传递队伍id
     *lichen 2019/4/9
     */
    public function getCert(){
        $user_id = input();
        //  //验证器验证
        (new GetCert)->goCheck();#验证参数isd
        $res = new BaseException();//实例化类
        $api = $this->api;
        //根据用戶id user_id 获取线路 判断线路的状态是否完成
        $liveStatus = NodeModel::getLiveStatus($user_id['user_id']);
        if(!$liveStatus){
            $apiName = '路线还未全部完成';
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        }
        //根据用户id获取用户信息
        $user = NodeModel::getUser($user_id['user_id']);
        $api['Data']['title'] = '热爱中国共产党';
        //根据用户id查询组员信息
        $teamData = NodeModel::getTeam($user_id['user_id']);
        $ids = explode(',',$teamData[0]['teams']);
        //根据ids查询队员数据
        $teamDatas = NodeModel::getTeams($ids);
        $teams = '';
        //将组员nickname组合成一个字符串
        foreach($teamDatas as $value){
                $teams .=$value['nickname'].'、';
        }
        //除去最后一个 '、'号
        $teams = rtrim($teams, "、");
        $api['Data']['username'] = $user['nickname'] ;
        $api['Data']['username1'] =$teams ;

        //根据路线id live_id查询路线
        $live = NodeModel::getLive($user_id['user_id']);
        $time = $live['update_time'];
        $times = explode(' ',$time);
        $times = explode('-',$times[0]);
        for($i = 1;$i < count($times);$i++){
            if($times[$i] < 10){
                $times[$i] = substr($times[$i],1);
            }
        }
        $time = $times[0].'年'.$times[1].'月'.$times[2].'日';
        $api['Data']['time'] = $time;
        $api['Data']['desc'] = $liveStatus['desc'];
        //获取完成线路的描述
        $api['Data']['desc'] = '于'.$time.$liveStatus['desc'];//考虑修改中...
        $apiName= '线路完成，获取证书';

        $apiData = $res->apiData(0, $api, $apiName);
        return json($apiData);

    }
    //添加场馆
    public function addNode(){
        (new NodeValidate())->goCheck();
        $res = new BaseException();//实例化类
        $apiName = '添加线路';
        $api = $this->api;
        $data = input('post.');
        $Node = new NodeModel();
        $status = $Node->NodeAdd($data);
        if (empty($status)) {
            $apiData = $res->apiData(400, $api, $apiName);//添加失败
            return json($apiData);
        } else {
            $apiData = $res->apiData(0, $api, $apiName);//添加成功
            return json($apiData);
        }
    }
    // 删除场馆信息
    public function delNode(){
        $id = input('post.id');
        (new DelNode)->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $data = NodeModel::delNode($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
    public function editNode(){
        $id = input('post.id');
        $data = input('post.');
        (new DelNode)->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '修改数据';
        $data = NodeModel::editNode($id,$data);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
    public function delNodeAll(){
        $id = input('post.id');
        (new DelNode())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $id = explode(',', $id);
        $data = NodeModel::delNode($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
}