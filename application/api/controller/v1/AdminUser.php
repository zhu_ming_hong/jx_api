<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/19
 * Time: 9:35
 */


namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\validate\AddAdminUser;
use app\api\validate\DeleteAdminUser;
use app\api\validate\Updateavatar;
use app\api\validate\ForgetPassword;
use app\api\validate\Updatemenu;
use app\api\validate\Login as LoginValidate;
use app\api\model\AdminUser as modeladminuser;
use app\lib\exception\AdminUserException;
use app\api\validate\UpdateAdminUser;
use app\lib\exception\DeleteAdminUser as deleadminuserException;
use app\lib\exception\BaseException;
use think\Controller;
use think\Request;

#后台用户管理接口

class AdminUser extends BaseController
{

    /**
     * 登录
     * @pram $name 账号
     * @pram @pwd  密码
     * @throws OrderException
     */
    public function Adminlogin()
    {
//        $id=1;
//        print_r($id);
        $postdata = input('post.');
//        print_r($postdata);
//        die;
        (new LoginValidate())->goCheck();
        $res = new BaseException();//调用公共方法
//        $result=[];
        $api = $this->api;
        $apiName = '登录';
        $username = trim($postdata['username']);
        $password = trim($postdata['password']);
        $data = modeladminuser::Selectusername($username); #判断用户是否存在
        if (empty($data) || !isset($data)) {#用户不存在
            $apiData = $res->apiData(20012, $api, $apiName);//该用户不存在
            return json($apiData);
        } else { #用户存在判断密码是否相等
            $password = md5(hash('sha256', $password));//密码加密
            if ($data['password'] == $password) { #密码相等登录成功
                #修改最后登录时间
                $update=modeladminuser::Updatelastlogintime($username);
                unset($data['password']);
                $api['Data'] = $data;

                $apiData = $res->apiData(0, $api, $apiName);//登录成功
                return json($apiData);

            } else {#密码不相等，登录失败
                $apiData = $res->apiData(20020, $api, $apiName);
                return json($apiData);
            }
        }

    }

    #新增用户
    public function  AddAdminUser()
    {
        (new AddAdminUser())->goCheck();#验证参数
        $postdata = input('post.');//获取所有参数
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '添加用户';
//        $apiName = iconv('gbk//TRANSLIT', 'utf-8', $apiName);
        #判断用户名是否已存在
        $name = modeladminuser::Selectusername($postdata['username']);
        if (!empty($name)) { #用户已存在，不能重复添加
            $apiData = $res->apiData(20024, $api, $apiName);//该用户已存在
            return json($apiData);
        } else { #用户不存在，可进行添加
            #添加用户信息
            $data = modeladminuser::AddAdminUser($postdata['site'],$postdata['username'], $postdata['nickname'], $postdata['password'], $postdata['email'], $postdata['status'], $postdata['role'], $postdata['mobile'],$postdata['avatar']);#添加后台用户
            if (empty($data)) {
                $apiData = $res->apiData(400, $api, $apiName);//添加失败
                return json($apiData);
            } else {
                $apiData = $res->apiData(0, $api, $apiName);//添加成功
                return json($apiData);
            }
        }

    }

    #获取用户列表
    public function  SelectAdminUser()
    {
//        (new AddAdminUser())->goCheck();#验证参数
        $data = modeladminuser::SelectAdminUser();
        unset($data['password']);
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取用户列表';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//查询失败
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);//获取用户列表成功
            return json($apiData);
        }
    }


    /*
     * 删除单条数据
     * 参数：id 用户id
     * 请求方式：post
     * */
    public function DeleteAdminUser()
    {
        $id = input('post.id');
        (new DeleteAdminUser())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $data = modeladminuser::DeleteAdminUser($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }

    /*
    * 删除多条数据
    * 参数：id 用户id(字符串格式传入：例如 1,2,3)
    * 请求方式：post
    * */
    public function DeleteAdmins()
    {
        $id = input('post.id');
        (new DeleteAdminUser())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $id = explode(',', $id);
        $data = modeladminuser::DeleteAdminUser($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }

    /*
    * 编辑用户数据
    * 参数：id 用户id
    * 请求方式：post
    * */
    public function UpdateAdminUser()
    {
        $data = input('post.');
        (new UpdateAdminUser())->goCheck();#验证参数
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '编辑用户';
        if(isset($data['password'])){
            $data['password'] = md5(hash('sha256',$data['password']));
            $info = modeladminuser::UpdateAdminUser($data);#修改数据
        }else{
            $info = modeladminuser::UpdateAdminUser($data);#修改数据
        }
        if (!$info || empty($info)) {
            $apiData = $res->apiData(400, $api, $apiName);//编辑用户数据失败
            return json($apiData);
        }
//        elseif($info=='admin'){
//            $apiData['msg']='超级管理员的用户名不能修改 id不能为1';
//            $apiData['error_code'] =300;s
//            return json($apiData);
//        }
        else{
            $apiData = $res->apiData(0, $api, $apiName);//编辑用户数据成功
            return json($apiData);
        }

        }


    /*
    * 上传用户头像
    * */
    public function Updateavatar()
    {
        (new Updateavatar())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '上传图片';
        $file=request()->file('file');
        $date=date("Ymd");
        if($file){
            $info = $file->move(ROOT_PATH . 'public/uploads/adminuser/');
            if($info){
                $img = '';
                // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
                $img=$info->getFilename();
                $apiData = $res->apiData(0, $api, $apiName);//图片上传成功
                $apiData['Data']['img']= '/uploads/adminuser/'.$date."/".$img;
                return json($apiData);
            }else{
            // 上传失败获取错误信息
//                return $file->getError();
            $apiData = $res->apiData(400, $api, $apiName);//图片上传失败
            return json($apiData);
         }
        }
//        $post['avatar'] = $this->getImg($_FILES['file']);//图片字段
//        if(isset($post['id'])){
//            $data = modeladminuser::Updateavatar($post['id'], $post['avatar']);
//            if ($data) {
//                $apiData = $res->apiData(0, $api, $apiName);//图片上传成功
//                return json($apiData);
//            } else {
//                $apiData = $res->apiData(400, $api, $apiName);//图片上传失败
//                return json($apiData);
//            }
//        }else{
//            $apiData = $res->apiData(0, $api, $apiName);//图片上传成功
//            $apiData['msg']='添加图片成功';
//            $apiData['error_code']=200;
//            return json($apiData);
//        }

    }

    /*
    * 忘记密码
    * */
    public function  ForgetPassword()
    {
        $post = input('post.');
        (new ForgetPassword())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $username = $post['username'];
        $fistpassword = trim(md5(hash('sha256', $post['fistpassword'])));//第一次输入的登录密码
        $lastpassword = trim(md5(hash('sha256', $post['lastpassword'])));//再次输入的登录密码
        $apiName = '修改密码';
        $users=modeladminuser::Selectusername($username);//查询用户信息
        if($users){
            if ($fistpassword == $lastpassword) {
                $updatepwd = modeladminuser::Updatepwd($username, $fistpassword);
                if ($updatepwd) {
                    $apiData = $res->apiData(0, $api, $apiName);//修改密码成功
                    return json($apiData);
                } else {
                    $apiData = $res->apiData(400, $api, $apiName);//修改密码失败
                    return json($apiData);
                }
            } else {
                $apiData = $res->apiData(20023, $api, $apiName);//两次输入的密码不一致
                return json($apiData);
            }

        }else{
            $apiData = $res->apiData(20012, $api, $apiName);//用户不存在
            return json($apiData);
        }


    }
    /*
      * 获取菜单列表
      * */
    public function  Selectmenu()
    {
//        (new AddAdminUser())->goCheck();#验证参数
        $data = modeladminuser::Selectmenu();
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取菜单列表';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//获取菜单列表失败
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);//获取菜单列表成功
            return json($apiData);
        }
    }

    /*
   * 新增菜单
   * */
    public function  Addmenu(){
        $post = input('post.');
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '新增菜单';
        $info=modeladminuser::Addmenu($post);//查询用户信息
        if($info){
            $apiData = $res->apiData(0, $api, $apiName);//新增菜单成功
            return json($apiData);
        }else{
            $apiData = $res->apiData(400, $api, $apiName);//新增菜单失败
            return json($apiData);
        }
    }
    /*
    * 修改菜单
    * */
    public function  Upadatemenu(){
        (new Updatemenu())->goCheck();#验证参数id
        $post = input('post.');
        $id=$post['id'];
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '修改菜单';
        $info=modeladminuser::Upadatemenu($id,$post);//查询用户信息
        if($info){
            $apiData = $res->apiData(0, $api, $apiName);//新增菜单成功
            return json($apiData);
        }else{
            $apiData = $res->apiData(400, $api, $apiName);//新增菜单失败
            return json($apiData);
        }
    }

    /*
   * 删除单条菜单
   * */
    public function  Deletemenu(){
        (new Updatemenu())->goCheck();#验证参数id
        $post = input('post.');
        $id=$post['id'];
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除单条菜单';
        $info=modeladminuser::Deletemenu($id);//查询用户信息
        if($info){
            $apiData = $res->apiData(0, $api, $apiName);//删除单条菜单成功
            return json($apiData);
        }else{
            $apiData = $res->apiData(400, $api, $apiName);//删除单条菜单失败
            return json($apiData);
        }
    }
    /*
    * 删除多条菜单
    * 参数：id 用户id(字符串格式传入：例如 1,2,3)
    * */
    public function  DeleteMenus(){
        (new Updatemenu())->goCheck();#验证参数id
        $id = input('post.id');
        $id = explode(',', $id);
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除多条菜单';
        $info=modeladminuser::Deletemenu($id);//查询用户信息
        if($info){
            $apiData = $res->apiData(0, $api, $apiName);//删除多条菜单
            return json($apiData);
        }else{
            $apiData = $res->apiData(400, $api, $apiName);//删除多条菜单
            return json($apiData);
        }
    }
}