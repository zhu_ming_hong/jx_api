<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/1/9
 * Time: 11:15
 */

namespace app\api\controller\v1;
use app\api\validate\Live as LiveValidate;
use app\api\validate\DelLive;
use app\api\validate\GetLive;
use app\api\controller\BaseController;
use app\lib\exception\BaseException;
use app\api\model\Live as LiveModel;

class Live extends BaseController
{

    public function getLive(){
        $user_id = input('post.');
        (new GetLive)->goCheck();#验证参数id
        $data = LiveModel::getLive($user_id['user_id']);
        $res = new BaseException();
        $api = $this->api;
        $apiName = '获取线路列表';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);
            return json($apiData);
        }
    }
    //添加场馆
    public function addLive(){
        (new LiveValidate())->goCheck();
        $res = new BaseException();//实例化类
        $apiName = '添加线路';
        $api = $this->api;
        $data = input('post.');
        $Live = new LiveModel();
        $status = $Live->LiveAdd($data);
        if (empty($status)) {
            $apiData = $res->apiData(400, $api, $apiName);//添加失败
            return json($apiData);
        } else {
            $apiData = $res->apiData(0, $api, $apiName);//添加成功
            return json($apiData);
        }
    }
    // 删除场馆信息
    public function delLive(){
        $id = input('post.id');
        (new DelLive)->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $data = LiveModel::delLive($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
    public function editLive(){
        $id = input('post.id');
        $data = input('post.');
        (new DelLive)->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '修改数据';
        $data = LiveModel::editLive($id,$data);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
    public function delLiveAll(){
        $id = input('post.id');
        (new DelLive())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $id = explode(',', $id);
        $data = LiveModel::delLive($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
}