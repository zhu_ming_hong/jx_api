<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/20
 * Time: 16:45
 */

namespace app\api\controller\v1;


use app\api\controller\BaseController;
use app\api\validate\AddAdminRole;
use app\lib\exception\BaseException;
use app\api\model\AdminUser as modeladminuser;
use app\api\model\AdminRole as modeladminrole;
use app\api\validate\DeleteAdminUser;

class AdminRole extends BaseController
{

    /** 根据角色id获取权限列表
     * @return \think\response\Json
     */
    public function getPermission(){
        $id = $this->request->param('id');
        $data = modeladminuser::getPermission($id);
//        print_r($data);die;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取角色id';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//查询失败
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);//获取用户列表成功
            return json($apiData);
        }
    }
    #获取角色列表
    public function  SelectAdminRole()
    {
//        (new AddAdminUser())->goCheck();#验证参数
        $data = modeladminuser::SelectAdminRole();
//        print_r($data);die;
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '获取用户列表';
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//查询失败
            return json($apiData);
        } else {
            $api['Data'] = $data;
            $apiData = $res->apiData(0, $api, $apiName);//获取用户列表成功
            return json($apiData);
        }
    }
    #新增角色
    public function  AddAdminRole()
    {
//        (new AddAdminRole())->goCheck();#验证参数
        $post = input('post.');//获取所有参数
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '添加用户';
        #添加角色信息
        $data = modeladminuser::AddAdminRole($post);//添加角色

        if (empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//添加失败
                return json($apiData);

            } else {

            $rid=$data;//角色id
            $access=modeladminuser::Addaccess($rid,$post['group']);//添加权限
            if($access){
                $apiData = $res->apiData(0, $api, $apiName);//添加成功
                return json($apiData);
            }else{
                $apiData = $res->apiData(400, $api, $apiName);//添加失败
                return json($apiData);
            }

            }
    }
    #修改角色
    public function  UpdateAdminRole()
    {
//        (new AddAdminRole())->goCheck();#验证参数
        $post = input('post.');//获取所有参数
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '修改角色';
        $rid=$post['id'];
        #添加角色信息
        $data = modeladminrole::UpdateAdminRole($post,$rid);//修改角色
        if (empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//修改失败
            return json($apiData);
        } else {
            $access=modeladminrole::Updateaccess($rid,$post['group']);//修改权限
            if($access){
                $apiData = $res->apiData(0, $api, $apiName);//修改成功
                return json($apiData);
            }else{
                $apiData = $res->apiData(400, $api, $apiName);//修改失败
                return json($apiData);
            }
        }
    }
    /*
  * 删除单条角色
  * 参数：id 用户id
  * 请求方式：post
  * */
    public function DeleteAdminRole()
    {
        $id = input('post.id');
        (new DeleteAdminUser())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $data = modeladminuser::DeleteAdminRole($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }
    /*
   * 删除多条角色
   * 参数：id 用户id(字符串格式传入：例如 1,2,3)
   * 请求方式：post
   * */
    public function DeleteRoles()
    {
        $id = input('post.id');
        (new DeleteAdminUser())->goCheck();#验证参数id
        $res = new BaseException();//实例化类
        $api = $this->api;
        $apiName = '删除数据';
        $id = explode(',', $id);
        $data = modeladminuser::DeleteAdminRole($id);#删除数据
        if (!$data || empty($data)) {
            $apiData = $res->apiData(400, $api, $apiName);//删除单条数据失败
            return json($apiData);
        }
        $apiData = $res->apiData(0, $api, $apiName);//删除单条数据成功
        return json($apiData);
    }

}