<?php
/**
 * Created by 七月.
 * Author: 七月
 * 微信公号：小楼昨夜又秋风
 * 知乎ID: 七月在夏天
 * Date: 2017/3/5
 * Time: 17:59
 */

namespace app\api\controller;


use app\api\service\Token;
use think\Controller;

class BaseController extends Controller
{
    /**
     * 生成guid（16位）
     * @return guid
     */
    function guid_16()
    {
        mt_srand((double)microtime() * 10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);
        $uuid = chr(123)
            . substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . chr(125);
        return strtolower(preg_replace("/-|{|}/", "", $uuid));
    }
    /**
     * 上传图片信息
     * @param $data
     * @return string
     */
    public function getImg($data)
    {
        $image = substr($data, strlen("data:image/jpeg;base64,"));
        $imageName = "25220_" . date("His", time()) . "_" . rand(1111, 9999) . '.png';
        if (strstr($image, ",")) {
            $image = explode(',', $image);
            $image = $image[1];
        }
        $path = "uploads/adminuser/" . date("Ymd", time());
        if (!is_dir($path)) { //判断目录是否存在 不存在就创建
            mkdir($path, 0777, true);
        }
        $imageSrc = $path . "/" . $imageName;  //图片名字
        file_put_contents(ROOT_PATH . "public/" . $imageSrc, base64_decode($image));//返回的是字节数
        return $imageSrc;
    }

    public $api = array('Data'=>[],'Status'=>false,'error_code'=>0,'msg'=>'');

//    protected function checkExclusiveScope()
//    {
//        Token::needExclusiveScope();
//    }
//
//    protected function checkPrimaryScope()
//    {
//        Token::needPrimaryScope();
//    }
//
//    protected function checkSuperScope()
//    {
//        Token::needSuperScope();
//    }
}