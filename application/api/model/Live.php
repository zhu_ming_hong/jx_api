<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/1/9
 * Time: 11:18
 */

namespace app\api\model;


use think\Db;

class Live extends BaseModel
{
    protected $autoWriteTimestamp = 'datetime';
    protected $table = 'qdx_admin_Live';
    public static function getLive($user_id){
        $data = self::where('status',1)->where('user_id',$user_id)->order('sort desc')->select();
        return json_decode($data);
    }
    // ���ӳ���
    public function LiveAdd($data){
        if($data) {
            $list = self::save($data);
            if($list){
                return $list;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    public static function delLive($id){
        $data = Db::table('qdx_admin_Live')->delete($id);
        return $data;
    }
    public static function editLive($id,$data){
        unset($data['id']);
        $data = Db::table('qdx_admin_Live')->where('id',$id)->update($data);
        return $data;
    }
}