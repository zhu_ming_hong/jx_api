<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/17
 * Time: 17:58
 */

namespace app\api\model;

use think\Model;
use think\Db;

class User extends BaseModel
{
    protected $autoWriteTimestamp = true;
    public static function addUserB($data){
        $list = Db::table('qdx_user')->insert($data);
        return $list;
    }
    /**
     * �޸��û���Ϣ
     * @param id
     *
     */
    public static function editUser($data){
        $id = $data['id'];
        unset($data['id']);
        $data = Db::table('qdx_user')->where('id',$id)->update($data);
        return $data;
    }
    /**
     *  ��ѯ�û���Ϣ�б�
     * @param user_id
     * @return int|string
     */
    public function SelectUser($user_id, $status){
    $data = Db::table('qdx_user')->where('site_id',$user_id)->where('status',$status)->select();
    return json_decode($data);
}
    /**
     *  ��ѯ�û���Ϣ
     * @param user_id
     * @return int|string
     */
    public function SelectUserFind($user_id){
        $data = Db::table('qdx_user')->where('id',$user_id)->find();
        return $data;
    }
    /**
     *  ��ѯ��������
     * @return int|string
     */
    public function SelectAbout(){
        $data = Db::table('qdx_about')->select();
        return json_decode($data);
    }
    /**
     *  ��ѯ�û���Ϣ�б�
     * @param user_id
     * @return int|string
     */
    public function SelectOpen($openid){
        $data = Db::table('qdx_user')->where('openid',$openid)->find();
        return $data;
    }
    /** ��ȡ�����û�
     * @param $id
     * @param $fistpassword
     * @return int|string
     */
    public static function SelectUserClose($site){
        $list = Db::table("qdx_user_site")->alias("a")->join('qdx_user b','a.user_id = b.id')->where('a.site_id',$site)->where('a.status',0)->select();
        return $list;
    }
    public function UpdatePwd($phone, $fistpassword){

        $info=['password'=>$fistpassword];
        $data = Db::table('qdx_user')->where('phone',$phone)->update($info);
        return $data;
    }
    /*
   * ��ȡ������Ϣ
   * */
    public function SelectScenic($id){
        $data = Db::table('qdx_scenicdisplay')->where('id',$id)->select();
//        return $data;
        return json_decode($data);
    }
    /*
     * ��ȡ��֤��
     * */
    public function Getyzm($mobile){

        $time=date('Y-m-d H:i:s');
        $result = db('dxfsjl')->order('tjsj desc')->where('mobile', $mobile)->where('mbbc','r_zcdl')->where('dqsj', '>=',$time)->find();
//        echo $this->getLastSql();die;
        return $result;
    }
    /*
    * �û�ע��
    * */
    public  function UserRegister($phone, $fistpassword, $nickname,$open_id){
        $time=date('Y-m-d H:i:s',time());
        $user = model('User');
        $user->data([
            'username'  => $phone,
            'phone'  =>  $phone,
            'password' =>  $fistpassword,
            'nickname' =>  $nickname,
//            'fitness' =>  $fitness,
            'openid' =>  $open_id,
//            'age' =>  $age,
            'create_time' =>$time
        ]);
        return $user->save();
    }
    /*
     * QQ��¼�û�ע��
     */
    public static function addUserQQ($nickname,$openid,$img){
        $time=date('Y-m-d H:i:s',time());
        $data = ([
            'nickname' => $nickname,
            'openid' => $openid,
            'img' => $img,
            'create_time' => $time,
        ]);
        $id = db::table('qdx_user')->insertGetId($data);
       return $id;
    }
    /*
     *����id��ѯ�û���Ϣ
     * 2019/4/11 ���
     */
    public static function getUserById($id){
        $data = db::table('qdx_user')->field('password',true)->find($id);
        return $data;
}
    /*
    * ��ȡ�û�Э��  ���������ı�
    */
    public static function getMessage($name){
        $data = db::table('qdx_about')->where('id',2)->field($name)->find();
        return $data;
    }
    /*
     * �û���¼��ѯ�û���Ϣ
     *
     */
    public  function UserLogin($phone){
        $data = Db::table('qdx_user')->where('phone',$phone)->find();
        return $data;
    }
    /*
     * ��ȡģ������
     * */
    public  function  getmbmc($mbbc){
        $result = db('dxmbgl')->order('gxsj desc')->where('mbbc', $mbbc)->where('isDisable', 0)->where('isDelete', 0)->find();
        return $result;
    }
    /**
     * @param $mobile
     * @return mixed
     * ����name ��ȡ��Աid
     */
    public function getId($name){
        $result = db::table('qdx_user')->where('name',$name)->field('id')->find();
        return $result;
    }
    /**
     * @param $mobile
     * @return mixed
     * ���ݶӳ�user_id ���¶�Աids
     */
    public function updateTeams($user_id,$teams){
        $result = db::table('qdx_team')->where('user_id',$user_id)->update(['teams'=>$teams]);
        return $result;
    }
    /**
     * @param $mobile
     * @return mixed
     * ���ݶӳ�id ���¶�Ա����
     */
    public function updateTeamName($user_id,$teamName){
        $result = db::table('qdx_team')->where('user_id',$user_id)->update(['name'=>$teamName]);
        return $result;
    }
    /*
    * ��ȡ����ʱ��
    * */
    public function getdxyzm($mobile)
    {
        $result = db('dxfsjl')->order('tjsj desc')->where('mobile', $mobile)->find();
        return $result;
    }
    /**
     * /**
     * ��Ӷ�Ϣ��¼ wc 2018-06-14
     * @param $mbbc ģ����
     * @return \think\response\Json
     */
    public function adddxfsjl($data)
    {
        $result = db('dxfsjl')->strict(false)->insert($data);
        return $result;
    }
    /**
     * ��¼��·
     */
    public static function addLives($data){
         // ��ѯ���ݿ��Ƿ����
        $lists = Db::table('qdx_live_save')->where('live_id',$data['live_id'])->where("user_id",$data["user_id"])->find();
        if(count($lists)<1){
            Db::table('qdx_live_save')->insert($data);
        }
         // ��ѯ��¼id�Ƿ����
         $ids = db::table("qdx_save_nodes")->where("live_id",$data['live_id'])->order("id desc")->find();
         if(count($ids)>0){
             return 1;
         } else {
             //��ѯ���½ڵ�id
             $node_ids = db::table("qdx_admin_nodes")->where("live",$data['live_id'])->order("id asc")->find();
             $nodes_list["live_id"]=$data['live_id'];
             $nodes_list["node_id"]=$node_ids["id"];
             Db::table('qdx_save_nodes')->insert($nodes_list);
         }
         return 1;
    }

    /**
     * @param $live_id
     * @return mixed
     */
    public static function getNodes($live_id){
            $data = db::table("qdx_admin_nodes")->where('status',1)->where('live',$live_id)->select();
            return json_decode($data);
        }
		
		public static function getNodess($activity_id){
        $data = db::table("qdx_admin_nodes")->field('id,title,img_url,desc')->where('status',1)->where('activity_id',$activity_id)->select();
        $data=$data->toArray($data);
       foreach ($data as $k=>$v){
           $data[$k]['img_url']=explode(',',$v['img_url']);
       }
        return $data;
    }
    /**
     * ��Ӷ�Ϣ��¼ wc 2018-06-14
     * @param $mbbc ģ����
     * @return \think\response\Json
     */
    public static function getsLive($data)
    {
        $result = Db::table('qdx_live_save')->alias("a")->join("qdx_admin_live b","a.live_id = b.id")->where("a.user_id",$data['user_id'])->field("b.title,a.live_id,a.status")->select();
        if(count($result)){
            $result=$result->toArray();
            foreach ($result as $k=>$v){
                $list = db::table("qdx_save_nodes")->field("node_id")->where("live_id",$v["live_id"])->order("id desc")->find();
                $result[$k]["node_id"]=0;
                $nodes=self::getNodes($v["live_id"]);
                $result[$k]["nodes"]=$nodes;
                if(count($list)>0){
                    $result[$k]["node_id"]=$list["node_id"];
                }
            }
            }
        return $result;
    }
    /**
     * ��ӽڵ�id
     */
    public function addNodes($data){
        // ��ѯ�ڵ��Ƿ����
        $list = db::table("qdx_save_nodes")->where("user_id",$data["user_id"])->where("live_id",$data["live_id"])->where("node_id",$data["node_id"])->find();
        if(count($list)>0){
            return 1;
        }else{
            $inserts['user_id']=$data['user_id'];
            $inserts['live_id']=$data['live_id'];
            $inserts['node_id']=$data['node_id'];
            $adds=db::table("qdx_save_nodes")->insert($inserts);
        }
        return $adds;
    }
    /**
     * �޸���·���״̬
     *
     */
    public function editLives($lives){
        $list = db::table("qdx_live_save")->where('user_id',$lives['user_id'])->where("live_id",$lives["live_id"])->update(["status"=>$lives['status']]);
        return $list;
    }
    /**
     * ��Ӷ�Ա
     *
     */
    public function addTeam($data){
       $list["user_id"] = $data["user_id"];
       $list["teams"] = $data["teams"];
       $adds= db::table("qdx_team")->insert($list);
       return $adds;
    }
    /**
     * ��ѯ�ӳ���Ϣ
     */
    public function getTeam($data){
        $list = db::table("qdx_user")->where("phone",$data['phone'])->field("id")->find();
        return $list;
    }
    public function getTeamList(){
        $list = db::table("qdx_team")->alias("a")->join("qdx_user b","a.user_id = b.id")->field("a.teams,b.username,a.user_id,b.phone")->select();
        return $list;
    }
    /**
     * ��ѯ�����Ϣ
     *
     */
    public function getTeamData($data){
        $list = db::table("qdx_team")->alias("a")->join("qdx_user b","a.user_id = b.id")->where('user_id',$data["user_id"])->field("a.teams,b.nickname,b.username,a.user_id,b.phone")->select();
        return json_decode($list);
    }
    /**
     * ɾ�������Ϣ
     */
    public static function delTeamAll($ids){
        $list = db::table("qdx_team")->delete($ids);
        return $list;
    }
    /***
     * ��ȡ���Ϣ
     * ��ҳ��ȡ����
     * ����ʱ��Ӧ�ô��ڵ�ǰʱ��
     */
    public static function activity( $start,$tatol,$time){
        $list = db::table("qdx_activity")
            ->where('end_time','> time',$time)
            ->field('id,title,name,start_time,img,nodes,end_time')
            ->order('id desc')
            ->limit($start,$tatol)
            ->select();

//        return json_decode($list);
        return $list;
    }

    /**
     * @return false|\PDOStatement|string|\think\Collection
     * ��ȡ������������������
     * �����㹻�ٽ��з�ҳ
     */
    public static function activitys($time){
        //�ж�ʱ��   ����ʱ�� ����  ��ǰʱ��
        $list = db::table("qdx_activity")->where('end_time','> time',$time)->count();
        return $list;
    }
    /**
     * ��ȡ�����
     */
    public static function activityOne($id){
        $list =   $list = db::table("qdx_activity")->where('id',$id)->field('id,title,name,start_time,img,desc,nodes')->find();
        return $list;
    }

}
