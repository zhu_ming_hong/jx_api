<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/1/9
 * Time: 11:18
 */

namespace app\api\model;


use think\Db;

class Site extends BaseModel
{
    protected $autoWriteTimestamp = 'datetime';
    protected $table = 'qdx_admin_site';
    public static function getSite(){
        $data = self::where('status',1)->select();
        return json_decode($data);
    }
    //ģ��ƥ�䳡��
    public function findLikeSite($msg){
        $list = db::table('qdx_admin_site')
            ->where('title','like',"%$msg%")
            ->whereOr('desc','like',"%$msg%")
            ->where('status',1)
            ->select();
        return json_decode($list);
    }
    //����sql
    public function findLikeSites($msg){
        $list = db::table('qdx_admin_site')
            ->where('title','like',"%$msg%")
            ->whereOr('desc','like',"%$msg%")
            ->where('status',1)
            ->select();
        $sql = $this->getLastSql();
        return $sql;
    }

    // ���ӳ���
    public function siteAdd($data){
        if($data) {
            $list = self::save($data);
            if($list){
                return $list;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    public static function delSite($id){
        $data = Db::table('qdx_admin_site')->delete($id);
        return $data;
    }
    public static function editSite($id,$data){
        unset($data['id']);
        $data = Db::table('qdx_admin_site')->where('id',$id)->update($data);
        return $data;
    }
}