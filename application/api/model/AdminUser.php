<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/19
 * Time: 10:07
 */

namespace app\api\model;

use think\Db;

class AdminUser extends BaseModel
{

    //获取菜单列表
    public static function Selectmenu()
    {
        $data = Db::table('qdx_admin_menu')->select();
        return json_decode($data);
    }

    //新增菜单
    public static function  Addmenu($data)
    {
        $time = $time = date('Y-m-d H:i:s', time());//获取当前时间
        $data['create_time']=$time;
        $info = Db::table('qdx_admin_menu')->insert($data);
        return $info;
    }
    //删除菜单
    public static  function Deletemenu($id){
        $data = Db::table('qdx_admin_menu')->delete($id);
        return $data;
    }
    //修改菜单
    public static function  Upadatemenu($id,$info)
    {
        $time = $time = date('Y-m-d H:i:s', time());//获取当前时间
        $info['update_time'] =$time;
        unset($info['id']);
        $data = Db::table('qdx_admin_menu')->where('id', $id)->update($info);
        return $data;
    }

//查询用户是否存在
    public static function  Selectusername($username)
    {
        $data = Db::table('qdx_admin_user')->where('username', $username)->find();
        return $data;
//        return json_decode($data);
    }


    //添加用户
    public static function  AddAdminUser($site,$username, $nickname, $password, $email, $status, $role, $mobile, $avatar)
    {
        $time = date('Y-m-d H:i:s', time());
        $password = md5(hash('sha256', $password));
        $user = model('admin_user');
        $user->data([
            'username' => $username,
            'site' => $site,
            'nickname' => $nickname,
            'password' => $password,
            'email' => $email,
            'status' => $status,
            'role' => $role,
            'avatar' => $avatar,
            'mobile' => $mobile,
            'create_time' => $time
        ]);
        return $user->save();
    }

    //获取用户列表
    public static function SelectAdminUser()
    {
        $data = Db::table('qdx_admin_user')->select();
        return $data;
    }

    //删除用户数据
    public static function DeleteAdminUser($id)
    {
        $data = Db::table('qdx_admin_user')->delete($id);
//        $user = model('admin_user');
//        $data=$user::destroy($id);
        return $data;
    }

    //修改最后登录时间
    public static function Updatelastlogintime($username)
    {
        $time = $time = date('Y-m-d H:i:s', time());//获取当前时间
        $info = ['last_login_time' => $time];
        $data = Db::table('qdx_admin_user')->where('username', $username)->update($info);
        return $data;
    }


    //编辑用户数据
    public static function UpdateAdminUser($data)
    {
        $time = $time = date('Y-m-d H:i:s', time());//获取当前时间

//        if($data['username']=='admin'){//当用户名为admin时可不修改
//            $info=['nickname'=>$data['nickname'],'password'=>$data['password'],'email'=>$data['password'],
//                'mobile'=>$data['password'],'sort'=>$data['password'],'status'=>$data['password'],'update_time'=>$time];
//        }else{
//            $info=['username'=>$username,'nickname'=>$nickname,'password'=>$password,'email'=>$email,
//                'mobile'=>$mobile,'sort'=>$sort,'status'=>$status,'update_time'=>$time];
//        }
//        print_r($data);die;
        $id = $data['id'];
        unset($data['id']);
        $data['update_time'] = $time;
        if ($id != 1) {
            $info = Db::table('qdx_admin_user')->where('id', $id)->update($data);
            return $info;
        } else {
            unset($data['username']);
            $info = Db::table('qdx_admin_user')->where('id', $id)->update($data);
            return $info;
        }
    }

    //上传图片
    public static function Updateavatar($id, $tp)
    {
        $info = ['avatar' => $tp];
        $data = Db::table('qdx_admin_user')->where('id', $id)->update($info);
        return $data;
    }

    //新增角色
    public static function AddAdminRole($post)
    {

        $time =date('Y-m-d H:i:s', time());//获取当前时间
        $post['create_time']=$time;
        $user=new AdminRole($post);
        $user->allowField(true)->save();
         return $user->id;
    }
    //添加权限
    public static  function Addaccess($rid,$group){
        $role = Db::table('qdx_admin_access');
        $time =  date('Y-m-d H:i:s', time());//获取当前时间
       $data=[
           'group' => $group,
            'rid' => $rid,
            'create_time' => $time,
       ];
        return $role->insert($data);
    }

    //删除角色
    public static function DeleteAdminRole($id)
    {
        $data = Db::table('qdx_admin_role')->delete($id);
        return $data;
    }

    //获取用户列表
    public static function SelectAdminRole()
    {
        $data = Db::table('qdx_admin_role')
            ->alias('r')
            ->join('qdx_admin_access s','r.id = s.rid','left')
            ->field('r.id,r.pid,r.name,r.description,r.menu_auth,r.sort,r.create_time,r.update_time,r.status,r.access,s.group')
            ->select();
        return json_decode($data);
    }
    // 根据角色id获取权限列表
    public static function getPermission($id){
        $data = Db::table('qdx_admin_access')->where('rid',$id)->find();
        return $data;
 }
    //修改密码
    public static function Updatepwd($username, $fistpassword)
    {
        $info = ['password' => $fistpassword];
        $data = Db::table('qdx_admin_user')->where('username', $username)->where('status', 1)->update($info);
        return $data;
    }

}