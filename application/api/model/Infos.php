<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/2/14
 * Time: 14:12
 */

namespace app\api\model;

use think\Db;
use think\Model;

class Infos extends BaseModel
{
    public static function addData($openid,$data){
        $data = Db::table("qdx_user")->where("openid",$openid)->update($data);
        return $data;
    }
}