<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/1/9
 * Time: 11:18
 */

namespace app\api\model;


use think\Db;

class Topic extends BaseModel
{
    protected $autoWriteTimestamp = 'datetime';
    protected $table = 'qdx_admin_Topics';

    /**
     * 根据id获取选择题
     */
    public static function getChoices($id){
        $data = Db::table('qdx_choice')->where('topic_id',$id)->order('id asc')->select();
        return json_decode($data);
    }
    /**获取题库
     * @param $user_id
     * @return mixed
     */
    public static function getTopic($user_id){
        $data = self::where('site_id',$user_id)->select();
        return json_decode($data);
    }
    public static function getTopics($user_id){
        $data = self::where('node_id',$user_id)->find();
        if($data){
            $data['choice'] = Db::table('qdx_choice')->where('topic_id',$data->id)->order('id asc')->select();
        }
        return json_decode($data);
    }
    // 添加题库
    public function TopicAdd($data){

        if($data) {
            $topics_choice = [];
            $choices = [];
            $topics_choice = $data['choices_topic'];
            unset($data['choices_topic']);
            $list = self::save($data);
            $topic_id = Db::name('qdx_admin_Topics')->getLastInsID();
            if($list){
               foreach ($topics_choice as $k=>$v){
                   $choices[$k]['topic_id'] = $topic_id;
                   $choices[$k]['choice'] = $v['choice'];
                   $choices[$k]['content'] = $v['content'];
                   $choices[$k]['create_time'] = date("Y-m-d :H:i:s");
               }
               if(Db::table('qdx_choice')->insertAll($choices)) {
                   return $list;
               }
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    public static function delTopic($id){
        $data = Db::table('qdx_admin_Topics')->delete($id);
        return $data;
    }
    public static function editTopic($id,$data){
        unset($data['id']);
        $choices_topic=$data['choices_topic'];
        unset($data['choices_topic']);
        $list = Db::table('qdx_admin_Topics')->where('id',$id)->update($data);
        if($data['choice'] == '1' && count($choices_topic) > 0){
            $choice = Db::table('qdx_choice')->where('topic_id',$id)->select();
            // 如果存在
            if (count($choice)>0){
                foreach ($choices_topic as $k=>$v){
                    Db::table('qdx_choice')->where('id',$v['id'])->update($v);
                }
            } else {
                //如果不存在
                foreach ($choices_topic as $k=>$v){
                    $choices[$k]['topic_id'] = $id;
                    $choices[$k]['choice'] = $v['choice'];
                    $choices[$k]['content'] = $v['content'];
                    $choices[$k]['create_time'] = date("Y-m-d :H:i:s");
                }
                if(Db::table('qdx_choice')->insertAll($choices)) {
                    return $list;
                }
            }

        } else {
            Db::table('qdx_choice')->where('topic_id',$id)->delete();
        }
        if($list !== false) {
            $list = true;
        }
        return $list;
    }
}