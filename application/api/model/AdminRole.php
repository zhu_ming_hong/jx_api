<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/28
 * Time: 17:37
 */

namespace app\api\model;

use think\Db;
class AdminRole extends BaseModel
{
    /*
     * 修改用户角色
     * */
    public static  function UpdateAdminRole($post,$rid){
        unset($post['id']);
        $user=new AdminRole();
        $time =date('Y-m-d H:i:s', time());//获取当前时间
        $post['update_time']=$time;
        $info=$user->allowField(true)->save($post,['id' =>$rid]);
        return $info;
    }
    /*
     * 修改权限
     * */
    public static  function Updateaccess($rid,$group){
        $time =date('Y-m-d H:i:s', time());//获取当前时间
        $info = ['group' => $group,'update_time'=>$time];
        $data = Db::table('qdx_admin_access')->where('rid', $rid)->update($info);
        return $data;
    }

}