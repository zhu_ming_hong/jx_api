<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/1/9
 * Time: 11:18
 */

namespace app\api\model;


use think\Db;

class Node extends BaseModel
{
    protected $autoWriteTimestamp = 'datetime';
    protected $table = 'qdx_admin_Nodes';
    public static function getNode($user_id){
    $data = self::where('status',1)->where('user_id',$user_id)->order('sort desc')->select();
//        $data = self::where('status',1)->where('user_id',$user_id)->order('sort desc')->select();
    return json_decode($data);
}
    public static function getNodes($live_id){
        $data = self::where('status',1)->where('live',$live_id)->order('sort asc')->select();
//        $data = self::where('status',1)->where('live',$live_id)->select();
        return json_decode($data);
    }

    /**
     * 根据队伍id获取队伍信息
     * lichen 2019/4/9
     */
    public static function getTeam($id){
        $data = db::table('qdx_team')->where('user_id',$id)->select();
        return $data;
    }
    /**
     * 根据队长id获取队员姓名
     * lichen 2019/4/9
     */
    public static function getTeamName($id){
        $result = db::table('qdx_team')->where('user_id',$id)->field('name')->find();
        return $result;
    }

    /**
     * 根据用户id查询用户信息
     */
    public static function getUser($id){
        $data = db::table('qdx_user')->where('id',$id)->find();
        return $data;
    }
    /**
     * 根据队员ids 查询队员信息
     */
    public static function getTeams($ids){
        $data = db::table('qdx_user')->where('id','in',$ids)->select();
        return $data;
    }
    /**
     * 根据节点id获取节点信息
     */
    public static function getLive($id){
        $data = db::table('qdx_live_save')->where('user_id',$id)->find();
        return $data;
    }
    /**
     * 根据队长id查询线路
     */
    public static function getLiveId($id){
        $data = db::table('qdx_live_save')->where('user_id',$id)->fired('live_id')->find();
        return $data;
    }
    /**
     * 根據用戶user_id判斷路線是否完成
     */
    public static function getLiveStatus($id){
        $data = db::table('qdx_live_save')->where('user_id',$id)->where('status',3)->find();
        return $data;
    }

    /**
     * @param $live_id
     * @return mixed
     * 获取最新节点id
     */
    public static function getNodeId($id){
        $data = db::table("qdx_save_nodes")->where('live_id',$id)->field("node_id")->order("id desc")->find();
        return $data;
    }
    // 添加场馆
    public function NodeAdd($data){
        if($data) {
            $list = self::save($data);
            if($list){

            }else{return $list;
                return 0;
            }
        }else{
            return 0;
        }
    }
    public static function delNode($id){
        $data = Db::table('qdx_admin_Nodes')->delete($id);
        return $data;
    }
    public static function editNode($id,$data){
        unset($data['id']);
        $list = Db::table('qdx_admin_Nodes')->where('id',$id)->update($data);
        if($list !== false) {
            $list = true;
        }
        return $list;
    }
}