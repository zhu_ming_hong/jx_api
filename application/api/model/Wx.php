<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2019/2/14
 * Time: 10:34
 */

namespace app\api\model;

use think\Model;
use think\Db;
class Wx extends Model
{
    /**
     * 检测是否有openid
     *
     */
    public static function hasOpenid($openid){
        $data = Db::table("qdx_user")->where("openid",$openid)->field('id')->find();
        return $data;
    }
    /**
     * 添加openid
     * @param $openid
     * @param $phone
     */
    public static function addOpenid($params){
        $data = Db::table("qdx_user")->insert($params);
        return $data;
    }
}