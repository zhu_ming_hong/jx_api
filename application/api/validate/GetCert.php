<?php
/**
 * Created by PhpStorm.
 * User: SDMY20180807
 * Date: 2019/4/9
 * Time: 14:03
 */

namespace app\api\validate;


class GetCert extends BaseValidate
{
    protected $rule = [
        'user_id' => 'require',
        'live_id' => 'require',
    ];
}