<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/19
 * Time: 9:46
 */

namespace app\api\validate;


class AddAdminUser extends BaseValidate
{
    protected $rule = [
        'username' => 'require',
        'nickname' => 'require|max:20|min:1',
        'password' => 'require',
        'email' => 'require|emailyz',
        'status' => 'require',
        'mobile' => 'require|isMobile',
        'role' => 'require|number',
    ];
}