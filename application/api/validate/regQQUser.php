<?php
/**
 * Created by PhpStorm.
 * User: SDMY20180807
 * Date: 2019/4/12
 * Time: 18:09
 */

namespace app\api\validate;


class regQQUser extends BaseValidate
{
    protected $rule = [
        'nickname' => 'require',
        'img' => 'require',
        'openid' => 'require',
    ];

}