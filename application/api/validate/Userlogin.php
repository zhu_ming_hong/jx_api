<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/26
 * Time: 14:21
 */

namespace app\api\validate;


class Userlogin extends BaseValidate
{
    protected $rule = [
        'phone' => 'require|isNotEmpty',
        'password'=>'require|isNotEmpty'
    ];

}