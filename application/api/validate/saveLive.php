<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/20
 * Time: 16:50
 */

namespace app\api\validate;


class saveLive extends BaseValidate
{
    protected $rule = [
        'user_id' => 'require|number',
        'live_id' => 'require|number'
    ];

}