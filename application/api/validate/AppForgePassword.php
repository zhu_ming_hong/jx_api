<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/26
 * Time: 10:39
 */

namespace app\api\validate;


class AppForgePassword extends BaseValidate
{
    protected $rule = [
        'phone' => 'require|isNotEmpty',
        'yzm' => 'require|isNotEmpty',
        'fistpassword'=>'require|isNotEmpty',
        'lastpassword'=>'require|isNotEmpty',
    ];

}