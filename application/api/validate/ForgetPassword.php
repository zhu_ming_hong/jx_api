<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/21
 * Time: 16:58
 */

namespace app\api\validate;


class ForgetPassword extends BaseValidate
{
    protected $rule = [
        'username' => 'require|isNotEmpty',
        'fistpassword'=>'require|isNotEmpty',
        'lastpassword'=>'require|isNotEmpty',
    ];
}