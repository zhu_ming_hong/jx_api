<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/19
 * Time: 11:40
 */

namespace app\api\validate;


class DeleteAdminUser extends  BaseValidate
{
    protected $rule = [
        'id' => 'require|min:1',
    ];

}