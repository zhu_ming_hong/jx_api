<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/21
 * Time: 15:04
 */

namespace app\api\validate;


class SendMessage extends BaseValidate
{
    protected $rule = [
        'mobile' => 'require|number|isMobile',
        'mbbc' => 'require',
    ];


}