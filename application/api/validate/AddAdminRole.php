<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/20
 * Time: 16:50
 */

namespace app\api\validate;


class AddAdminRole extends BaseValidate
{
    protected $rule = [
        'pid' => 'require|number',
        'name' => 'require',
        'description' => 'require',
        'sort' => 'require',
        'default_module' => 'require',
        'access' => 'require|number',
    ];

}