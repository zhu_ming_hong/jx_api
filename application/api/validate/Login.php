<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2018/12/18
 * Time: 14:25
 */

namespace app\api\validate;

class Login extends BaseValidate
{
    protected $rule = [
        'username' => 'require|isNotEmpty',
        'password'=>'require|isNotEmpty'
    ];
}