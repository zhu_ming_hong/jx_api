<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/19
 * Time: 9:46
 */

namespace app\api\validate;


class Wx extends BaseValidate
{
    protected $rule = [
        'openid' => 'require',
        'phone' => 'require'
    ];
}