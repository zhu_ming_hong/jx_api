<?php
/**
 * Created by PhpStorm.
 * User: SDMY-TC-201709
 * Date: 2018/12/18
 * Time: 15:08
 */

namespace app\api\validate;

use app\lib\exception\ParameterException;
use think\Exception;
class UserRegister extends BaseValidate
{
    protected $rule = [
        'phone' => 'require|isMobile|isNotEmpty',
        'nickname' => 'require|isNotEmpty',
    ];

}