<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/23 0023
 * Time: 下午 16:47
 */

namespace app\admin\model;

class LogLogin extends Base
{
    protected $autoWriteTimestamp = 'int';

    public function getCreateTimeAttr($value)
    {
        return date('Y-m-d H:i:s', $value);
    }
}
