<?php namespace app\admin\model;

class City extends Base
{
    public static function child($pid = 0)
    {
        return self::field('id, name')->where('pid', $pid)->select();
    }

    public static function areaText($prov = 0, $city = 0)
    {
        if (0 == $prov) {
            return '';
        }

        $area = self::column('name', 'id');
        $str  = $area[$prov];
        if (0 < $city) {
            $str .= ' ' . $area[$city];
        }

        return $str;
    }
}
