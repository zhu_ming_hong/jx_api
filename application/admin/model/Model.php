<?php
namespace app\admin\model;

/**
 * 模型管理
 */
class Model extends Base
{

    /**
     * 根据模型别名获取模型id
     * @staticvar array $lists
     * @param type $alias
     * @return type
     */
    public static function getId($alias)
    {
        static $lists = [];
        if (!isset($lists[$alias])) {
            $lists[$alias] = self::value(self::getPk());
        }
        return $lists[$alias];
    }

    public static function mid($alias)
    {
        return self::where('alias', $alias)->value('id');
    }
}
