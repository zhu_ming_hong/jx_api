<?php

namespace app\admin\model;

/**
 *
 * 模型基类
 * @author lxc
 */
class Balance extends \think\Model
{

    /**
     * 新增/更新操作
     * 描述:判断是否有主键如果有根据主键更新 没有为新增
     * @param type $data
     */
    public function edit($data)
    {
        $this->allowField(true);
        //判断更新或者新增操作->根据主键判断
        if (isset($data[$this->getPk()]) && !empty($data[$this->getPk()])) {
            $this->isUpdate = true;
        }
        return $this->save($data);
    }

}
