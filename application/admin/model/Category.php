<?php

namespace app\admin\model;

/**
 *
 * 栏目管理
 * @author fenghao <303529990@qq.com>
 */
class Category extends Base
{
    public static function child($pid = 0)
    {
        return self::field('id,title')->where('pid', $pid)->select();
    }
    /**
     * 查询权限列表
     * @param type $gid
     */
    public function role($rule)
    {
        $roles = $this->field('id,pid,title')->where('status', 1)->select();
        $lists = [];
        foreach ($roles as $key => $value) {
            $value = $value->toArray();
            if ($value['pid'] == 0) {
                $value['checked'] = $rule && strpos($rule, ',' . $value['id'] . ',') > -1 ? 'checked' : '';
                $value['sub']     = $this->tree($roles, $value['id'], $rule);
                $lists[]          = $value;
            }
        }
        return $lists;
    }

    /**
     * 获取子级权限
     * @param type $tree
     * @param type $pid
     * @param type $rule
     * @return type
     */
    private function tree($tree, $pid, $rule)
    {
        $newarr = array();
        foreach ($tree as $key => $value) {
            $value = $value->toArray();
            if ($value['pid'] == $pid) {
                $value['checked'] = $rule && strpos($rule, ',' . $value['id'] . ',') > -1 ? 'checked' : '';
                $value['sub']     = $this->tree($tree, $value['id'], $rule);
                $newarr[]         = $value;
            }
        }
        return $newarr;
    }

}
