<?php

return	array(	
	'system'=>array('name'=>'平台','icon'=>'home','child'=>array(

				array('name' => '基本设置','act'=>'base','op'=>'System','child' => array()),
				array('name' => '角色管理','act'=>'lists','op'=>'Role','child'=>array()),
	            array('name' => '栏目管理','act'=>'lists','op'=>'Menu','child'=>array()),
	            array('name' => '管理员日志','act'=>'logs','op'=>'System','child'=>array()),

	)),

    'user'=>array('name'=>'用户','icon'=>'user','child'=>array(
        array(
              'name' => '用户列表','act'=>'lists','op'=>'User','child' => array(
              'name' => '用户添加','act'=>'add','op'=>'User',
              'name' => '用户编辑','act'=>'edit','op'=>'User',
              'name' => '导出','act'=>'export','op'=>'User',
        )),
        array('name' => '团队管理','act'=>'lists','op'=>'Team','child'=>array()),
    )),

    'order'=>array('name'=>'订单','icon'=>'template','child'=>array(

        array(

            'name' => '订单列表','act'=>'lists','op'=>'Order','child' => array(
            'name' => '用户添加','act'=>'add','op'=>'User',
            'name' => '匹配订单','act'=>'matching','op'=>'User',
            'name' => '用户编辑','act'=>'edit','op'=>'User',
            'name' => '导出','act'=>'export','op'=>'User',

        )),

    )),

    'farm'=>array('name'=>'农场管理','icon'=>'app','child'=>array(

        array(

            'name' => '用户列表','act'=>'lists','op'=>'farm','child' => array(
            'name' => '添加种子','act'=>'add','op'=>'User',
            'name' => '添加金苹果','act'=>'edit','op'=>'User',

        )),
    )),
    'match'=>array('name'=>'交易管理','icon'=>'senior','child'=>array(

        array(

            'name' => '设置匹配员','act'=>'set_match','op'=>'match','child' => array(
            'name' => '设置成匹配员','act'=>'edit_user','op'=>'User',
            'name' => '取消匹配员','act'=>'cancel_user','op'=>'User',

        )),
        array('name' => '手动匹配','act'=>'index','op'=>'match','child'=>array(
            'name' => '开始匹配','act'=>'start_match','op'=>'User',
        )),
    )),
    'bill'=>array('name'=>'资产流水','icon'=>'form','child'=>array(

        array(
            'name' => '发布购买记录','act'=>'buy_lists','op'=>'bill','child' => array(
        )),
        array(
            'name' => '发布出售记录','act'=>'sold_lists','op'=>'bill','child' => array(
        )),
        array(
            'name' => '收货钱包','act'=>'receive_wallet_list','op'=>'bill','child' => array(
        )),
        array(
            'name' => '奖金钱包','act'=>'invite_wallet_list','op'=>'bill','child' => array(
        )),

    )),

/*    'set'=>array('name'=>'修改密码','icon'=>'set','child'=>array(
        array(
            'name' => '修改密码','act'=>'password','op'=>'set','child' => array(
        )),
    ))*/



);