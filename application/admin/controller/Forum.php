<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/10 0010
 * Time: 13:54
 */

namespace app\admin\controller;

use app\api\model\Comment;
use app\api\model\Member;
use app\api\model\Subject;
use app\api\model\SubjectMemberGx;
use app\api\model\SubjectShareLog;
use php\Slpic;
use think\Cache;
use think\Db;

class Forum extends Base
{

    //获取全部主题列表    或   通过UID 获取用户主页发贴的列表
    public function get_list()
    {
        if (request()->isAjax()) {
            $page = request()->param('page');//页数
            $url = $this->url;
            $list =Db::table('mxkj_member')
                ->join('mxkj_subject', 'mxkj_member.id=mxkj_subject.uid')
                ->where($this->_map)
                ->order('mxkj_subject.id desc')->paginate(10, false, ['page' => $page]);
            $count = $list->total();
            $total = ceil($list->total() / 10);//获取总页数
            $data = $list->all();
            $list_data = [];
            foreach ($data as $d) {
                $user = Member::get($d['uid']);
                $d['nickname'] = $user['nickname'];
                $d['head_img'] = $url . '/uploads/headimg/' . $user['head'];
                $d['create_time'] = date('Y-m-d H:i:s', $d['create_time']);
                $img_list_str = "";
                if (!empty($d['imgs']) || $d['imgs'] != '') {
                    $newstr = substr($d['imgs'], 0, strlen($d['imgs']) - 1);
                    if (!empty($newstr) && $newstr != "") {
                        $d['imgs'] = explode(",", $newstr);
                        foreach ($d['imgs'] as $v) {
                            $img_list_str .= $url . '/uploads/subject/' . $v . ",";
                        }
                        $d['imgs'] = $img_list_str;
                    }
                }
                $list_data[] = $d;
            }
            $this->ajaxReturn('', $list_data, $count);
        } else {

            return $this->fetch('lists');
        }
    }

    //查看图片
    public function look()
    {
        $param = request()->param();
        $count = count($param);
        if ($count == 2) {
            $imgs = $param['imgs'];
            $imgs = explode(",", $imgs);
            $this->assing('url',$this->url);
            $this->assign('imgs', $imgs);
            return $this->fetch('look_pic');
        } else {
            if (request()->isAjax()) {
                $subject_id = Cache::get('subject_id');
                $uid = 1;

                $url = $this->url;
                $mode = Subject::get($subject_id);
                $mode['create_time'] = date('Y-m-d H:i:s', $mode['create_time']);
                $img_list_str = "";

                if (!empty($mode['imgs'])) {
                    foreach ($mode['imgs'] as $v) {
                        $img_list_str .= $url . '/uploads/subject/' . $v . ",";
                    }
                    $mode['imgs'] = $img_list_str;
                }
                $list = Db::table('mxkj_member')
                    ->join('mxkj_comment', 'mxkj_comment.uid = mxkj_member.id')->where('subject_id', $subject_id)->order('mxkj_comment.id desc')->paginate($param['limit'], false, ['page' => $param['page']]);
                $total = ceil($list->total() / $param['limit']);//获取总页数
                $data = $list->all();

                $user = db('member')->where('id', $mode['uid'])->find();
                $mode['head'] = $url . '/uploads/headimg/' . $user['head'];
                $mode['nickname'] = $user['nickname'];

                //判断是否收藏和举报过
                $count = db('subject_member_gx')->where('uid', $uid)->where('subject_id', $subject_id)->count('id');
                $mode['is_collect'] = $count ? 1 : 0;
                $count = db('subject_report_gx')->where('uid', $uid)->where('subject_id', $subject_id)->count('id');
                $mode['is_report'] = $count ? 1 : 0;

                $list_data = [];
                /*                $list_data['subject'] = $mode;
                                $list_data['totalPage'] = $total;*/


                foreach ($data as $d) {
                    $d['head_img'] = $url . '/uploads/headimg/' . $d['head'];
                    $d['create_time'] = date('Y-m-d H:i:s', $d['create_time']);
                    /*                    $list_data['list'][] = $d;*/
                    $list_data[] = $d;
                }
                return $this->ajaxReturn('成功', $list_data,count($data), '0');
            } else {
                Cache::set('subject_id', $param['id'], 3600);
                return $this->fetch('comment');
            }
        }

    }

    //举报帖子列表
    public function report_list()
    {
        if (request()->isAjax()) {
            $page = request()->param('page');//页数
            $url = $this->url;
            $res = db('subject_report_gx')->field('subject_id')->group('subject_id')->select();
            $ids[] =-1;

            if ($res) {
                foreach ($res as $k => $v) {

                    $ids[] = $v['subject_id'];
                }
            }
            $where['id'] = array('in',$ids);
            $list = db('subject')->where($where)->order('id desc')->paginate(10, false, ['page' => $page]);
            $count = $list->total();
            $total = ceil($list->total() / 10);//获取总页数
            $data = $list->all();
            $list_data = [];
            foreach ($data as $d) {
                $user = Member::get($d['uid']);
                $d['nickname'] = $user['nickname'];
                $d['head_img'] = $url . '/uploads/headimg/' . $user['head'];
                $d['create_time'] = date('Y-m-d H:i:s', $d['create_time']);
                $img_list_str = "";
                if (!empty($d['imgs']) || $d['imgs'] != '') {
                    $newstr = substr($d['imgs'], 0, strlen($d['imgs']) - 1);
                    if (!empty($newstr) && $newstr != "") {
                        $d['imgs'] = explode(",", $newstr);
                        foreach ($d['imgs'] as $v) {
                            $img_list_str .= $url . '/uploads/subject/' . $v . ",";
                        }
                        $d['imgs'] = $img_list_str;
                    }
                }
                $report_counts = db('subject_report_gx')->where('subject_id', $d['id'])->count();
                $d['report_counts'] = $report_counts;
                $list_data[] = $d;
            }
            $this->ajaxReturn('', $list_data, $count);
        } else {
            return $this->fetch('forum/report/lists');
        }
    }
    //删除帖子
    public function report_del()
    {
            $param = request()->param();
            $res = db('subject')->where('id',$param['id'][0])->update(['status'=>0]);
            $this->ajaxReturn('封贴成功','','',0);
    }

    //删除帖子
    public function delete_subject()
    {
        $sub = db('subject')->where('id', $this->_param['id'])->find();
        if (empty($sub)) $this->ajaxReturn("帖子不存在", 201);

        Db::name('subject')->delete($sub);

        $this->ajaxReturn("删除成功", 200);
    }

    //删除评论
    public function delete_comment()
    {
        $sub = db('comment')->where('id', $this->_param['id'])->find();
        if (empty($sub)) $this->ajaxReturn("评论不存在", 201);

        Db::name('comment')->delete($sub);

        $this->ajaxReturn("删除成功", 200);
    }

    //查看评论图片
    public function show_img($id){
        $res =  Subject::get(['id'=>$id]);
        $img =  $res['imgs'];
        $data_img =json_encode($img);
        $this->assign('data_img',$data_img);
        $this->assign('list',$res);
        $this->assign('url1',$this->url);
        return $this->fetch('');

    }

}