<?php

namespace app\admin\controller;

use think\DB;

class Item extends Base
{
    //发现场景 列表
    public function lists()
    {
        if (request()->isAjax()){
            $list = Db::name('item_bank_scenic')->order('id desc');
            $total = $list->count();
            $list->limit(($this->_param['page'] - 1) * $this->_param['limit'], $this->_param['limit']);
            $where = $this->_map;
            unset($where['page']); unset($where['op']); unset($where['limit']);
            (!empty($where)) ? $data = $list->whereor($where)->select() : $data = $list->select();
            foreach ($data as $k => $v){
               $data[$k]['live'] =  Db::name('admin_scenic')->where('id',$v['subordinate'])->value('title');
            }
            $this->ajaxReturn('', $data, $total);
        } else {
            return $this->fetch('item/lists');
        }
    }

    //修改题库 显示页面
    public function edit_show($id)
    {
        $res = Db::name('item_bank_scenic')->where('id',$id)->find();
        $this->assign('user',$res);
        return $this->fetch('item/liveform');
    }

    //修改题库
    public function edit()
    {
      $data =  request()->post();
      $res  =  Db::name('item_bank_scenic')->where('id',$data['id'])->update($data);
      if($res){
          $this->success('编辑成功!');
      }else{
          $this->success('没有修改!');
      }
    }

    //添加题库 渲染视图
    public function add()
    {
        return $this->fetch('item/addform');
    }

    //添加题库 保存数据库
    public function add_item()
    {
        $data = request()->post();
        $res  = DB::name('item_bank_scenic')->insert($data);
        if($res){
            $this->success('添加成功');
        }else{
            $this->success('添加失败');
        }
    }


}