<?php

namespace app\admin\controller;

use think\Controller;
use app\admin\model\WalletBill;
use app\admin\model\WalletBillCatepory;
use app\admin\model\Member;

class Wallet extends Base
{
    public function lists()
    {
        if (isset($this->_param['op']) && $this->_param['op'] == 'table') {
            $list = db('wallet_bill')->order('id desc');
            $list->limit(($this->_param['page'] - 1) * $this->_param['limit'], $this->_param['limit']);
            $list2 =    clone $list;
            $to=$list2->count();
            $where = $this->_map;
            unset($where['page']);unset($where['op']);unset($where['limit']);
            (!empty($where)) ? $data = $list->whereor($where)->select() : $data = $list->select();
            $this->assign('map', $this->_param);
            foreach ($data as $k => $v){
                $user = Member::get(['id' => $v['uid']]);
                if(empty($user)) {unset($data[$k]); continue;}
                $data[$k]['nickname'] = $user->nickname;
                $WalletBillCatepory = WalletBillCatepory::get(['id' => $v['category_id']]);
                $data[$k]['category_name'] = $WalletBillCatepory->name;
            }
            $this->ajaxReturn('', $data,$to);
        }
        if (!isset($this->_param['op'])){
            return $this->fetch('Wallet/lists');
        }
    }
    //专家列表
    public function expert_list()
    {
        if (isset($this->_param['op']) && $this->_param['op'] == 'table'){
            $list = db('wallet_expert_bill')->order('id desc');
            $total = $list->count();
            $list->limit(($this->_param['page'] - 1) * $this->_param['limit'], $this->_param['limit']);
            $where = $this->_map;
            unset($where['page']);unset($where['op']);unset($where['limit']);
            (!empty($where)) ? $data = $list->whereor($where)->select() : $data = $list->select();
            $this->assign('map', $this->_param);
            foreach ($data as $k => $v){
                $user = new Member;
                $user = Member::get(['id' => $v['expert_uid']]);
                $data[$k]['nickname'] = $user->real_name;
            }
            $this->ajaxReturn('', $data, $total);
        }
        if (!isset($this->_param['op'])){
            return $this->fetch('Wallet/expert_list');
        }
    }
}
