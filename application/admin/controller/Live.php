<?php

namespace app\admin\controller;

use think\DB;

class Live extends Base
{
    //发现场景 列表
    public function lists()
    {
        if (request()->isAjax()) {
            $list = Db::name('admin_activity')->order('id desc');
            $total = $list->count();
            $list->limit(($this->_param['page'] - 1) * $this->_param['limit'], $this->_param['limit']);
            $where = $this->_map;
            unset($where['page']);
            unset($where['op']);
            unset($where['limit']);
            (!empty($where)) ? $data = $list->whereor($where)->select() : $data = $list->select();
            $this->assign('map', $this->_param);
            foreach ($data as $k => $v) {

            }
            $this->ajaxReturn('', $data, $total);
        } else {
            return $this->fetch('live/lists');
        }
    }

    //查看详情
    public function info($id)
    {
        $list = DB::name('admin_activity')->where('id', $id)->find();
        $this->assign('list', $list);
        return $this->fetch('live/info');
    }

    //删除线路
    public function del()
    {
        $id = $this->_param['id'];
        $res = DB::name('admin_activity')->where('id', $id)->delete();
    }

    //修改
    public function edit($id)
    {
        $res = Db::name('admin_activity')->where('id', $id)->find();
        $this->assign('user', $res);
        return $this->fetch('live/liveform');
    }

    //景区列表
    public function scenic_list()
    {
        if (request()->isAjax()) {
            $list = Db::name('admin_scenic')->order('id desc');
            $total = $list->count();
            $list->limit(($this->_param['page'] - 1) * $this->_param['limit'], $this->_param['limit']);
            $where = $this->_map;
            unset($where['page']);
            unset($where['op']);
            unset($where['limit']);
            (!empty($where)) ? $data = $list->whereor($where)->select() : $data = $list->select();
            $this->assign('map', $this->_param);
            foreach ($data as $k => $v) {

            }
            $this->ajaxReturn('', $data, $total);
        } else {
            return $this->fetch('live/lists');
        }
    }


}