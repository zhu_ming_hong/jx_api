<?php

namespace app\admin\controller;

use app\admin\model\AuthGroup;
use app\admin\model\AuthRule;
use think\Controller;

/**
 *
 * 角色管理
 * @author fenghao <303529990@qq.com>
 */
class Role extends Base
{

    /**
     * 获取角色列表
     * @return type
     */
    public function lists()
    {
        if(request()->isAjax()){
            $lists = AuthGroup::where($this->_map)->order($this->_order)->select();

            $this->ajaxReturn('',$lists,count($lists));
        }else{
            $lists = AuthRule::where($this->_map)->where('pid',0)->order($this->_order)->select();
            foreach ($lists as $k=>$v){
                $son = AuthRule::where('pid',$v['id'])->order('sort')->select();
                if(!empty($son)){
                    foreach ($son as $ka=>$va){
                        $son[$ka]['son'] = AuthRule::where('pid',$va['id'])->order('sort')->select();
                    }
                }
                $lists[$k]['son'] = $son;
            }
            $this->assign('parents_menu', json_encode($lists));
            return $this->fetch();
        }
    }

    //添加角色
    public function add()
    {

        return $this->_edit();
    }

    /**
     * 修改角色
     * @return type
     */
    public function edit($id)
    {
        return $this->_edit($id);
    }

    private function _edit($id = '')
    {
        //print_r($this->_param);die;
        if ($this->request->isPost()) {
            $rule = [
                ['title', 'require|length:1,32', '角色名称不能为空|角色名称必须在1~32字符之间'],
            ];
            if (!empty($this->_param['rules'])) {
                $this->_param['rules'] = ',' . implode(',', $this->_param['rules']) . ',';
            }
            $msg = $this->validate($this->_param, $rule);
            if ($msg !== true) {
                $this->error($msg, '');
            }
            $auth_rule = new AuthGroup();
            if ($auth_rule->edit($this->_param) !== false) {
                $this->success(empty($id) ? '新增角色成功!' : '更新角色成功!', url('lists'));
            } else {
                $this->error($auth_rule->getError(), '');
            }
        } else {
            $info = [];
            if (!empty($id)) {
                $info = AuthGroup::get($id)->toArray();
            }
            $AuthRule = new AuthRule();
            $roles    = $AuthRule->role(isset($info['rules']) ? $info['rules'] : []);

            $managers = AuthGroup::where($this->_map)->order($this->_order)->select();
            $this->assign('managers', $managers);

            $this->assign('roles', $roles);
            $this->assign('info', $info);
            return $this->fetch('edit');
        }
    }

    /**
     * 启用/禁用
     * @param type $id
     * @param type $status
     */
    public function status($id, $status)
    {
        $res = AuthGroup::where('id', $id)->update(['status' => $status]);
        if ($res !== false) {
            cache('system_menu', null);
            $this->success($status == 1 ? '启用成功' : '禁用成功', url('lists'));
        } else {
            $this->error($status == 1 ? '启用失败' : '禁用失败', '');
        }
    }

    /**
     * 自动排序
     * @param type $id
     * @param type $sort
     */
    public function sort($id, $sort)
    {
        $res = AuthGroup::where('id', $id)->update(['sort' => $sort]);
        if ($res !== false) {

            $this->success('排序成功!', url('lists'));
        } else {
            $this->error('排序失败!', '');
        }
    }

    //批量删除用户
    public function del($id)
    {
        $res = AuthGroup::destroy($id);
        if ($res !== false) {
            $this->success('删除成功!', url('lists'));
        } else {
            $this->error('删除失败!', '');
        }
    }

}
