<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\session;
class Set extends Base

{
    public function password()
    {
        if(request()->isAjax()){
            $id = Session::get('user_auth');
            $id = $id['id'];
            $new_password = $this->_param['password'];
            $new_password = think_ucenter_md5($new_password, MANAGER_AUTH_KEY);
            $res = db('manager')->where('id',$id)->update(['password' => $new_password]);
            if($res){
                session('user_auth', null);
                session('user_auth_sign', null);
                $this->ajaxReturn('','修改成功');
            }
        }else{
            return view();
        }
    }

    public function login()
    {
        if (is_login()) {
            $this->redirect('index/index');
        }
        return $this->fetch();
    }

}
