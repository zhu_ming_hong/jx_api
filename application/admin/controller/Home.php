<?php
namespace app\admin\controller;

use think\Controller;

class Home extends Controller
{
    //控制台
    public function console()
    {
        return $this->fetch();
    }
    //主页1
    public function homepage1()
    {
        return $this->fetch();
    }
    //主页2
    public function homepage2()
    {
        return $this->fetch();
    }
}
