<?php
namespace app\admin\controller;

use think\Controller;
use app\admin\model\Config;
class System extends Base
{
    //控制台
    public function base()
    {
        $data = db('config')->where('group',1)->select();
        $lists = [];
        foreach ($data as $k=>$v){
            $lists[$v['name']] = $v['value'];
        }
        $this->assign('lists',$lists);
        return $this->fetch('lists');
        
    }
    public function edit(){
        if(!empty($this->_param)){
            $config = new Config();
            foreach ($this->_param as $k=>$v){
                if(trim($v)!=''){
                    if($config->GET(['name'=>$k])){
                        $config->where(['name'=>$k])->update(['value'=>$v]);
                    }else{
                        $config->insert(['name'=>$k,'value'=>$v,'group'=>1]);
                    }
                    
                }
            }
            
        }
        $this->success('设置完成!', url('base'));
    }
    public function update_app()
    {
        if ($this->request->isPost()) {
            /* print_r($_FILES['apk_file']);
             print_r($_FILES['ipa_file']);print_r($this->_param);die;  */
            $rule = [
                ['apk_version', 'require', '安卓版本号不能为空'],
                ['ipa_version', 'require', '苹果版本号不能为空'],
            ];
    
            $msg = $this->validate($this->_param, $rule);
            if ($msg !== true) {
                $this->error($msg, '');
            }
            $data = $this->_param;
            if(isset($data['apk_version'])){
                $origin_str = file_get_contents(APP_PATH.'config.php');
                if(config("VERSION")['apk']['version']){
                    $val = config("VERSION")['apk']['version'];
                    $update_str = str_replace("'apk'=>['update'=>true,'version'=>'{$val}'","'apk'=>['update'=>true,'version'=>'{$data['apk_version']}'", $origin_str);
                    file_put_contents(APP_PATH.'config.php', $update_str);
                    //print_r($data);print_r(config("VERSION"));die;
                }
            }
            if(isset($data['ipa_version'])){
                $origin_str = file_get_contents(APP_PATH.'config.php');
                if(config("VERSION")['ios']['version']){
                    $val = config("VERSION")['ios']['version'];
                    $update_str = str_replace("'ios'=>['update'=>true,'version'=>'{$val}'","'ios'=>['update'=>true,'version'=>'{$data['ipa_version']}'", $origin_str);
                    file_put_contents(APP_PATH.'config.php', $update_str);
                }
            }
            $apk = true;$ipa= true;
            if(!empty($_FILES['apk_file']['name'])){
                $apk_dir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
                if(!file_exists($apk_dir))mkdirs($apk_dir);
                $file = $_FILES['apk_file'];
                $tmp_file = $file ['tmp_name'];
                $file_types = explode(".",$file['name']);
                $file_type = $file_types [count($file_types) - 1];
                $file_names = $file_types[0];
                $file_name = $apk_dir.'/'.$file_names.'.apk';
                if (!in_array(strtolower($file_type), array('apk'))) {
                    $this->error('不是要求apk文件类型，请重新选择！','');
                }
                if(copy($file['tmp_name'],$file_name)){
    
                }else{
                    $apk = false;
                }
            }
            if(!empty($_FILES['ipa_file']['name'])){
                $ipa_dir = $_SERVER['DOCUMENT_ROOT'] . '/download/';
                if(!file_exists($ipa_dir))mkdirs($ipa_dir);
                $file = $_FILES['ipa_file'];
                $tmp_file = $file ['tmp_name'];
                $file_types = explode(".",$file['name']);
                $file_type = $file_types [count($file_types) - 1];
                $file_names = $file_types[0];
                $file_name = $ipa_dir.'/'.$file_names.'.ipa';
                if (!in_array(strtolower($file_type), array('ipa'))) {
                    $this->error('不是要求ipa文件类型，请重新选择！','');
                }
                if(copy($file['tmp_name'],$file_name)){
    
                }else{
                    $ipa = false;
                }
            }
            if($apk&&$ipa){
                $this->success('保存成功');
            }elseif($apk){
                $this->error('苹果版本上传失败', '');
            }elseif($ipa){
                $this->error('安卓版本上传失败', '');
            }else{
                $this->error('上传失败', '');
            }
        }else{
             
            $data = config('version');
            $this->assign('data',$data);
            return $this->fetch();
        }
    }
}
