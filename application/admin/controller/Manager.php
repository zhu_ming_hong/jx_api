<?php

namespace app\admin\controller;

use app\admin\model\AuthGroup;
use app\admin\model\AuthRule;
use app\admin\model\Manager as Ma;
use think\Controller;

/**
 *
 * 角色管理
 * @author fenghao <303529990@qq.com>
 */
class Manager extends Base
{

    /**
     * 获取角色列表
     * @return type
     */
    public function lists()
    {
        if(request()->isAjax()){
            $lists = db('manager')->where($this->_map)->order($this->_order)->select();
            foreach ($lists as $k=>$v){
                $v['reg_time'] = empty($v['reg_time'])?'':date('Y-m-d H:i:s',$v['reg_time']);
                //print_r($v);
                $v['last_login_time'] = empty($v['last_login_time'])?'':date('Y-m-d H:i:s',$v['last_login_time']);
                $v['last_login_ip'] = long2ip($v['last_login_ip']);
                $v['title'] = db('auth_group')->where('id',$v['role'])->value('title');
                $lists[$k] = $v;
            }
            $this->ajaxReturn('',$lists,count($lists));
        }else{
            $lists = AuthRule::where($this->_map)->where('pid',0)->order($this->_order)->select();
            foreach ($lists as $k=>$v){
                $son = AuthRule::where('pid',$v['id'])->order('sort')->select();
                if(!empty($son)){
                    foreach ($son as $ka=>$va){
                        $son[$ka]['son'] = AuthRule::where('pid',$va['id'])->order('sort')->select();
                    }
                }
                $lists[$k]['son'] = $son;
            }
            $this->assign('parents_menu', json_encode($lists));
            return $this->fetch();
        }
    }
    public function role(){
        $role = db('auth_group')->where('status',1)->select();
        $this->ajaxReturn('',$role,count($role));
    }
    //添加角色
    public function add()
    {

        return $this->_edit();
    }

    /**
     * 修改角色
     * @return type
     */
    public function edit($id)
    {
        return $this->_edit($id);
    }

    private function _edit($id = '')
    {
        //print_r($this->_param);die;
        if ($this->request->isPost()) {
            if(!empty($this->_param['id'])){
                $rule = [
                    ['username', 'require|alphaNum|length:4,12', '账户名称不能为空|账户名称只能为数字或字母|账户名称必须在4~12字符之间'],
                    ['role', 'require', '角色不能为空'],
                ];
            }else{
                $rule = [
                    ['username', 'require|alphaNum|length:4,12', '账户名称不能为空|账户名称只能为数字或字母|账户名称必须在4~12字符之间'],
                    ['password', 'require|alphaNum|length:6,20', '密码不能为空|密码只能为数字或字母|密码必须在6~20字符之间'],
                    ['role', 'require', '角色不能为空'],
                ];
            }
            $msg = $this->validate($this->_param, $rule);
            if ($msg !== true) {
                $this->error($msg, '');
            }
            if(!empty($this->_param['password'])){
                if($this->_param['password']!=$this->_param['repeat_password']){
                    $this->error('两次密码输入不同', '');
                }
            }
            $manager = new Ma();
            $id_check = db('manager')->where('username',$this->_param['username'])->find();
            if($id_check){
                if(empty($id)){
                    $this->error('账户名已存在', '');
                }elseif($id_check['id']!=$id){
                    $this->error('账户名已存在', '');
                }
            }
            if ($manager->edit($this->_param) !== false) {
                $this->success(empty($id) ? '新增成功!' : '更新成功!', url('lists'));
            } else {
                $this->error('编辑失败', '');
            }
        } 
    }

    /**
     * 启用/禁用
     * @param type $id
     * @param type $status
     */
    public function status($id, $status)
    {
        $res = AuthGroup::where('id', $id)->update(['status' => $status]);
        if ($res !== false) {
            cache('system_menu', null);
            $this->success($status == 1 ? '启用成功' : '禁用成功', url('lists'));
        } else {
            $this->error($status == 1 ? '启用失败' : '禁用失败', '');
        }
    }

    /**
     * 自动排序
     * @param type $id
     * @param type $sort
     */
    public function sort($id, $sort)
    {
        $res = AuthGroup::where('id', $id)->update(['sort' => $sort]);
        if ($res !== false) {

            $this->success('排序成功!', url('lists'));
        } else {
            $this->error('排序失败!', '');
        }
    }

    //批量删除用户
    public function del($id)
    {

        $res = Manager::destroy($id);
        if ($res !== false) {

            $this->success('删除成功!', url('lists'));
        } else {
            $this->error('删除失败!', '');
        }
    }

}
