<?php
namespace app\admin\controller;

use app\admin\model\Manager;
use think\Session;
use think\Db;
class Admin extends \think\Controller
{
    /**
     * 登录
     * @author yangqilin <444266105@qq.com>
     * @return type
     */
    public function login()
    {
        if (is_login()) {
            $this->redirect('index/index');
        }

        if ($this->request->isPost()) {
            
            $rule = [
                ['username', 'require', '账号不能为空'],
                ['password', 'require', '密码不能为空'],
                ['code', 'require', '验证码不能为空'],
            ];
            $msg = $this->validate($this->request->post(), $rule);
            if ($msg !== true) {
                $this->error($msg, '');
            }
            $manage = new Manager();
            $res    = $manage->login($this->request->post('username'), $this->request->post('password'), $this->request->post('code'));
            if ($res === false) {
                //$this->error($manage->getError(), '');
                echo json_encode(array('code'=>1,'msg'=>$manage->getError()));exit;
            }else {
                echo json_encode(array('code'=>0,'msg'=>'登录成功','data'=>array('access_token'=>'')));exit;
                
            }
        } else {
            return $this->fetch();
        }
    }
    /**
     *
     * 退出
     * @author fenghao <303529990@qq.com>
     */
    public function logout()
    {
        if (is_login()) {
            session('user_auth', null);
            session('user_auth_sign', null);
        }
        $this->success('退出成功!', url('admin/login'));
    }

    public function base()
    {
        $manager = new Manager();
        $data = $manager->find($this->uid);
        $this->assign('data',$data);
        return $this->fetch();
    }

    public function updatePassword()
    {
        $user_res = session::get();
        $res = $user_res['user_auth'];
        $this->assign('data', $res);
        return $this->fetch();
    }

    public function updatePasswords()
    {
        $past_password = isset($_POST['past_password']) ? $_POST['past_password'] : '';
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $where = [
            'id' => $user_id,
            'password' => think_ucenter_md5($past_password, MANAGER_AUTH_KEY)
        ];
        $res = Db::name('manager')->where($where)->find();
        if ($res) {
            $arr = [
                'message' => 200,
                'res' => $res
            ];
        } else {
            $arr = [
                'message' => 500
            ];
        }
        return json($arr);
    }

    public function updatePasswordL()
    {
        $past_password = isset($_POST['passwords']) ? $_POST['passwords'] : '';
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $data = [
            'password' => think_ucenter_md5($past_password, MANAGER_AUTH_KEY)
        ];
        $res = Db::name('manager')->where('id=' . $user_id)->setField($data);
        if ($res) {
            $arr = [
                'message' => 200
            ];
        } else {
            $arr = [
                'message' => 500
            ];
        }
        return json($arr);
    }
}

