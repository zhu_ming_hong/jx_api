<?php
namespace app\lib\exception;
use think\Exception;

/**
 * Class BaseException
 * 自定义异常类的基类
 */
class BaseException extends Exception
{
    public $code = 400;
    public $msg = 'invalid parameters';
    public $errorCode = 999;
    public $api = array('Data'=>[],'Status'=>false,'error_code'=>0,'msg'=>'');
    public $shouldToClient = true;

    /**
     * 构造函数，接收一个关联数组
     * @param array $params 关联数组只应包含code、msg和errorCode，且不应该是空值
     */
    public function __construct($params=[])
    {
        if(!is_array($params)){
            return;
        }
        if(array_key_exists('code',$params)){
            $this->code = $params['code'];
        }
        if(array_key_exists('msg',$params)){
            $this->msg = $params['msg'];
        }
        if(array_key_exists('errorCode',$params)){
            $this->errorCode = $params['errorCode'];
        }
    }

    public function Common($code,$msg,$errorCode,$data){
        $arr=array();
        $arr['code']=$code;
        $arr['msg']= iconv('gbk//TRANSLIT','utf-8',$msg);
//        $arr['msg']= $msg;
        $arr['errorCode']=$errorCode;
        $arr['result']=$data;
        return $arr;
    }
    /**
     * 接口数据重构
     * @param $StatusCode 错误编码
     * @param $apiData 返回给前台json数据
     * @param string $apiName api接口名称
     * @param string $isEmpty 获取数据是 'empty'为空
     * @return mixed
     */
    public function apiData($StatusCode, $api, $apiName = '', $isEmpty = '')
    {
        if ($StatusCode == 0) {
            $api['Status'] = true; //请求成功
        } else {
            $api['Status'] = false; //请求失败
        }
        $api['error_code'] = $StatusCode; //错误编码
        $api['msg'] = $this->errorMessage($StatusCode, $apiName, $isEmpty); //通过errorCode查找提
        return $api;
    }

    public function errorMessage($code, $apiName = '', $isEmpty)
    {
        $errorCode = array(
            0 => $apiName . '成功',
            400 => $apiName . '失败',
            10000 => '系统繁忙，请稍后再试',
            10001 => '系统错误或没有数据或该字段是唯一字段',
            10002 => '服务暂停',
            10003 => '远程服务错误',
            10004 => '限制不能请求该资源',
            10005 => '参数错误，请参考API文档',
            10006 => '非法请求',
            10007 => '缺失必选参数，请参考API文档',
            10008 => '参数值非法，需为 (%s)，实际为 (%s)，请参考API文档',
            10009 => '请求长度超过限制',
            10010 => '必须用Post方式请求',
            10011 => '请求API不存在',
            10012 => '验证码错误',
            10013 => '短信发送失败',
            20010 => '参数为空',
            20011 => 'Uid参数为空',
            20012 => '用户不存在',
            20013 => '不支持的图片类型，仅仅支持JPG、GIF、PNG',
            20014 => '图片太大',
            20016 => '此IP地址上的行为异常',
            20017 => '需要验证码',
            20018 => '验证错误',
            20019 => '认证失败',
            20020 => '用户名或密码不正确',
            20021 => '数据不存在',
            20022 => '请求不允许',
            20023 => '两次输入的密码不一致',
            20024 => '该用户已存在',
            20025 => '该商品已存在',
            20026 => '库存不足',
            20027 => '新密码与旧密码相同',
            20028 => '用户密码错误',
            20029 => '该用户地址已超过上限',
            20030 => '该订单已评价，不能重复评价',
            20031 => '该用户与该地址信息不符合',
            20032 => '修改头像失败',
            20033 => '手机号检测失败，该号已被绑定',
            20034 => '订单不存在或该订单不属于你',
            20035 => '数据保存失败',
            20036 => 'code无效,请重新获取',
            20037 => 'token无效,请重新获取',
            20038 => '消息为空',
            20039 => '订单发布成功,短信通知工人失败,请手动联系工人,谢谢!',
            20040 =>'已经申诉了,切勿重复提交!',
            20041 =>'暂无该类型的工人!',
            20042 => '没有更多了',

        );
//    如果是空列表进行提醒
        if ($isEmpty == 'empty') {
            return $apiName . '为空';
        }
        return $errorCode[$code];
    }

}

