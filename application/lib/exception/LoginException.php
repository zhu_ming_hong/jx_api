<?php
/**
 * Created by PhpStorm.
 * User: QF002
 * Date: 2018/12/18
 * Time: 14:32
 */

namespace app\lib\exception;


class LoginException extends BaseException
{
    public $code = 400;
    public $msg = '登录失败';
    public $errorCode = 80000;
}