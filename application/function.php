<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/1 0001
 * Time: 10:35
 */

//图片压缩
function image_png_size_add($imgsrc,$imgdst){
    header("Access-Control-Allow-Origin:*");
    vendor('phpqrcode.phpqrcode');
    list($width,$height,$type)=getimagesize($imgsrc);

    $new_width = ($width>600?600:$width)*0.5;

    $new_height =($height>600?600:$height)*0.5;

    switch($type){

        case 1:

            $giftype=check_gifcartoon($imgsrc);

            if($giftype){

                header('Content-Type:image/gif');

                $image_wp=imagecreatetruecolor($new_width, $new_height);

                $image = imagecreatefromgif($imgsrc);

                imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

                imagejpeg($image_wp, $imgdst,75);

                imagedestroy($image_wp);

            }

            break;

        case 2:

            header('Content-Type:image/jpeg');

            $image_wp=imagecreatetruecolor($new_width, $new_height);

            $image = imagecreatefromjpeg($imgsrc);

            imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

            imagejpeg($image_wp, $imgdst,75);

            imagedestroy($image_wp);

            break;

        case 3:

            header('Content-Type:image/png');

            $image_wp=imagecreatetruecolor($new_width, $new_height);

            $image = imagecreatefrompng($imgsrc);

            imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

            imagejpeg($image_wp, $imgdst,75);

            imagedestroy($image_wp);

            break;

    }

}

function check_gifcartoon($image_file){

    $fp = fopen($image_file,'rb');

    $image_head = fread($fp,1024);

    fclose($fp);

    return preg_match("/".chr(0x21).chr(0xff).chr(0x0b).'NETSCAPE2.0'."/",$image_head)?false:true;

}

function  addEtag($file) {
    $last_modified_time = filemtime($file);
    $etag = md5_file($file);
    // always send headers
    header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
    header("Etag: $etag");
    // exit if not modified
    if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time ||
        @trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag) {
        header("HTTP/1.1 304 Not Modified");
        exit;
    }
}
