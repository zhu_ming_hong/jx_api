<?php
return [
    /**
     * 文件转换配置配置
     */
    'file' => [
        //文件保存路径
        'path'      => './uploads/files/',
        //新文件保存根路径
        'new_path'  => '/home/www/yyt_api/',
        //压缩文件临时保存根路径
        'temp_path' => ROOT_PATH.'runtime/music/',
        //旧文件保存根路径
        'old_path'  => '/home/www/yyt_old/',

    ],
];
